import React, {Component} from "react";
import Navbar from "./navbar";
import {Route,Redirect,Switch} from "react-router-dom";
import Login from "./login";
import Logout from "./logout";
import Admin from "./admin";
import Employee from "./employee";
import auth from "./services/authService";
import Employees from "./viewEmployees";
import Bills from "./bills";
import AddContact from "./addContact";
import AddEmployee from "./addEmployee";
import EmpDepartmentDetails from "./empDepartmentDetails";
import HotelBill from "./hotelBill";
import TravelBill from "./travelBill";
import NotAllowed from "./notAllowed";

class MainComponent extends Component {
    state={};


    render() {
        const user = auth.getUser();
        return (
            <div className="container">
            <Navbar user={user}/>
            <Switch>
                <Route path='/login' component={Login}/>
                <Route path='/logout' component={Logout}/>
                <Route path='/emp/hotelbill/:billid'  render={props=> user ? user.role==="EMPLOYEE" ?  <HotelBill {...props} user={user}/> : <Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path='/emp/travelbill/:billid'  render={props=> user ? user.role==="EMPLOYEE" ?  <TravelBill {...props} user={user}/>  :<Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path="/emp/bills" render={props=> user ? user.role==="EMPLOYEE" ? <Bills {...props} user={user}/>:<Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path="/emp/contact" render={props=> user ? user.role==="EMPLOYEE" ?  <AddContact {...props} user={user} /> :<Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path='/emp' render={props=> user ? user.role==="EMPLOYEE" ? <Employee {...props} user={user}/> : <Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path="/admin/viewemp/:empuserid" render={props=> user ?  user.role==="ADMIN" ?   <EmpDepartmentDetails {...props}/>: <Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path="/admin/addemp" render={props=> user ?  user.role==="ADMIN" ? <AddEmployee {...props}/>:<Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/> }/>
                <Route path='/admin/viewemp' render={props=> user ? user.role==="ADMIN" ? <Employees {...props}/> :<Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/> }/>
                <Route path='/admin' render={props=> user ? user.role==="ADMIN" ? <Admin {...props} user={user}/> : <Redirect to={`/notAllowed`}/>:<Redirect to={`/login`}/>}/>
                <Route path="/notAllowed" render={props=><NotAllowed /> }/>

                <Redirect path="/" to="/login"/>
            </Switch>    
            </div>
        )
    }
}
export default MainComponent;