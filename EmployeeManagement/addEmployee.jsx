import React, {Component} from "react";
import http from "./services/httpService";
class AddEmployee extends Component {
    state={
        employee:{ role:"EMPLOYEE" },
        errors:{},
    };


    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        if(input.name !=="password2")  
        s1.employee[input.name] = input.value;
        s1.errors[input.name] = this.makeFormErrors(input.name,input.value);
        this.setState(s1);
    }

    makeFormErrors=(name,value)=> {

        let password1 = this.state.employee.password;
        switch(name) {

            case "name":
                return value 
                ? value.length >=8 
                ? ("")
                :"Name should have atleast 8 characters"                                                                           
                :"Name is mandatory";  
            case "email":
                return value 
                ?value.includes('@') && value.includes('.com')
                ?("")
                :"Not a Valid Email"
                :"Email is mandatory";
            case "password":
                return value 
                ?""
                :"Password is mandatory";
            case "password2":
                return password1===value 
                ?""
                :"Passwords do not Match!"
        }
    }

    async handleSubmit() {
        try {
            let response = await http.post(`/empapp/emps`,this.state.employee);
             let detailsStatus = "submitted";
             this.setState({detailsStatus:detailsStatus});
        }catch(ex) {

        }

    }

    render() {
        let {employee,errors={},detailsStatus} = this.state;
        let {name,email,password,password2} = employee;
        
        return (
            <div className="container text-center bg-light">
                <h3>Welcome to Employee Management Portal</h3>
                  <h5 className="font-weight-bold mb-3">Add New Employee</h5>  
                  {detailsStatus && <div className="text-success">Employee Successfully added</div> }
                   {this.makeTextBox("Name","Enter Name","name",name,errors.name)}
                   {this.makeTextBox("Email","Enter Email","email",email,errors.email)}
                   {this.makeTextBox("Password","Enter Password","password",password,errors.password)}
                   {this.makeTextBox("","Re-Enter Password","password2",password2,errors.password2)}
                   <button className="btn btn-primary py-1 font-weight-bold rounded-0" disabled={detailsStatus==="submitted"} onClick={()=>this.handleSubmit()}>Add</button>
            </div>
        )
    }


    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-4">
                    <label>{label}</label>
                </div>
                <div className="col-6">
                    <input
                        type={name==="text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default AddEmployee;