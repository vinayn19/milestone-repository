import React, {Component} from "react";
import http from "./services/httpService";
class AddContact extends Component {
    state={
        empContactDetails:{},
        errors:{},
    };

    async componentDidMount() {
        let {empuserid} = this.props.user;
        let response = await http.get(`/empapp/empcontact/${empuserid}`);
        let {data:empContactDetails} = response;
        if(Object.keys(empContactDetails).length===6) 
            this.setState({detailsStatus:true,empContactDetails:empContactDetails})
        else 
            this.setState({detailsStatus:false,empContactDetails:empContactDetails})
            
    }

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        s1.errors[input.name] = this.makeFormErrors(input.name,input.value);
        s1.empContactDetails[input.name] = input.value;
        this.setState(s1);
    }
 makeFormErrors=(name,value)=> {
        switch(name) {
            case "mobile":
                return value 
                ?  +value.length>=10 && value.includes('+') && value.includes('-') && value.includes(' ')
                ? ""
                :"Mobile number is atleast 10 digits long and consists of digits(0-9),+,-and space"
                :"Mobile number is mandatory";
            case "country":
                return value 
                ? ""
                :"Country is required";
            case "pincode":
                return value 
                ? ""
                :"Pincode is required";
        }
    }

    async handleSubmit(empuserid) {
        let response = await http.post(`/empapp/empcontact/${empuserid}`,this.state.empContactDetails);
        this.setState({detailsStatus:"submitted"});
    }

    render() {
        let {empContactDetails,detailsStatus,errors} = this.state;
        let {country,pincode,address,city,mobile,empuserid} = empContactDetails;
        // let {mobile,country,pincode} = errors;
        return (
            <div className="container text-center">
                <h4>Welcome to Employee Management Portal</h4>
                <h5>Your Contact Details</h5>
                {detailsStatus ?
                        detailsStatus==='submitted' ?
                     <div className="text-success">Details have been successfully added</div>:
                     <div className="text-success">Displaying Contact Details</div>
                     :<div className="text-danger">No Contact Details Found. Please Enter Them.</div>  }
                {this.makeTextBox("Mobile","Enter mobile number","mobile",mobile,errors.mobile)}
                {this.makeTextBox("Address","Enter address","address",address)}
                {this.makeTextBox("City","Enter city","city",city)}
                {this.makeTextBox("Country","Enter country","country",country,errors.country)}
                {this.makeTextBox("Pincode","Enter pincode","pincode",pincode,errors.pincode)}
                <button className="btn btn-primary py-1 font-weight-bold rounded-0" disabled={detailsStatus} onClick={()=>this.handleSubmit(empuserid)}>Submit</button>
                
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-4">
                    <label>{label}</label>
                </div>
                <div className="col-6">
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default AddContact;
