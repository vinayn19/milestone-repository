import React, {Component} from "react";
import http from "./services/httpService";
class Bills extends Component {
    state={
        data:{},
        showForm:false,
        formData:{},
        formErrors:{},
        expenseTypeArr:["Travel", "Hotel", "Software", "Communication", "Others"],
    };

    async fetchData() {
        let {empuserid} =  this.props.user;
        let response = await http.get(`/empapp/empbills/${empuserid}`)
        let {data} = response;
        this.setState({data:data});
    }

     componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps,prevState) {
        if(prevProps!==this.props) {
            this.fetchData();
        }
    }

    showBillForm=()=> {
        this.setState({showForm:true});
    }

    handleChange=(e)=> {
        let {currentTarget: input} = e;
        let s1 = {...this.state};
        s1.formData.empuserid = this.props.user.empuserid;
        s1.formData[input.name] = input.value;
        s1.formErrors[input.name] = this.makeFormErrors(input.name,input.value); 
        this.setState(s1);
    }

    makeFormErrors=(name,value)=> {
        switch(name) {
            case "description":
                return value 
                ? ""
                :"Description is mandatory";
            case "expensetype":
                return value 
                ? ""
                :"Expense is mandatory";
            case "amount":
                return +value 
                ? ""
                :"Not a valid amount";
        }
    }

    validateForm=()=> {
        return Object.values(this.state.formData).length===4 && Object.values(this.state.formErrors).findIndex(value=> value!=="")==-1;
    }

    async submitBillsForm(){
        let {empuserid} = this.props.user;
        let response = await http.post(`/empapp/empbills/${empuserid}`,this.state.formData);
        this.state.billSubmitted = true; 
        this.props.history.push('/emp/bills');

    }

    handleBillDetails=(expensetype,billid)=> {
        switch (expensetype) {
            case "Hotel":
                 this.props.history.push(`/emp/hotelbill/${billid}`)
                 break;
            case "Travel":
                this.props.history.push(`/emp/travelbill/${billid}`)
                break;
        }

    }

    render() {
        let {data,showForm,expenseTypeArr,formData,formErrors,billSubmitted} = this.state;
        let {data:bills=[],pageInfo} = data;
        let {description,amount,expensetype} = formData;
        console.log(bills);
        return (
            <React.Fragment>
                <div className="text-center">
                    <h3>Welcome to the Employee Management Portal</h3>
                    <h5 className="text-left">Details of Bills Submitted</h5>
                    <div className="row text-light" style={{backgroundColor:"#35586C"}}>
                        <div className="col-3 border">Id</div>
                        <div className="col-3 border">Description</div>
                        <div className="col-3 border">Expense Head</div>
                        <div className="col-2 border">Amount</div>
                        <div className="col-1 border"></div>
                    </div>
                    
                    {bills.map((bill)=> 
                    (<div className="row text-center">
                        <div className="col-3 border">{bill.billid}</div>
                        <div className="col-3 border">{bill.description}</div>
                        <div className="col-3 border">{bill.expensetype}</div>
                        <div className="col-2 border">{bill.amount}</div>
                        <div className="col-1 border" onClick={()=>this.handleBillDetails(bill.expensetype,bill.billid)}>{(bill.expensetype==="Hotel" || bill.expensetype==="Travel") && <i className="fa fa-plus-square border"></i>}</div>
                        
                    </div>
                    ))}
                </div>
                  <a href="#" className="text-left" onClick={this.showBillForm}>Submit a New Bill</a>  
                    {showForm &&    
                        <div className="container text-center bg-light">
                            {billSubmitted && <div className="text-success">New bill have been successfully created</div> }
                            {this.makeTextBox("Description","Enter description","description",description,formErrors.description)}
                            {this.makeTextBox("Amount","Enter amount","amount",amount,formErrors.amount)}
                            {this.makeDropDowns("Expense Type",expenseTypeArr,expensetype,"expensetype","Select Expense",formErrors.expensetype)}
                        <button className="btn btn-primary mt-3 btn-sm" disabled={!this.validateForm()} onClick={()=>this.submitBillsForm()}>Submit</button>
                        </div> 
                    }
            </React.Fragment>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-4">
                    <label>{label}</label>
                </div>
                <div className="col-6">
                    <input
                        type={name==="text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )

    makeDropDowns = (label,arr,value,name,header,error="") => (
        <React.Fragment>
        <div className="row d-flex justify-content-center">
            <div className="col-4" style={{height:"38px"}} >
               <label>{label}</label> 
            </div>
            <div className="col-6" style={{height:"38px"}}>
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map(opt => (<option >{opt}</option>))}
            
                    </select>
    
                </div>
            </div>
        </div>
                    <div className="d-flex justify-content-center" >
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

                      </div>  

        </React.Fragment>
    )
}
export default Bills;