import axios from "axios";

const baseApiURL = "http://localhost:2410";

function get(url) {
    return axios.get(baseApiURL + url);
}

function post(url,obj){
    return axios.post(baseApiURL + url,obj);
}

function put(url,obj) {
    return axios.put(baseApiURL + url,obj);
}

function deleteReq(url) {
    return axios.delete(baseApiURL + url);
}

export default {
    get,
    post,
    put,
    deleteReq
}