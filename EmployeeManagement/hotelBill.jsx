import React, {Component} from "react";
import http from "./services/httpService";
class HotelBill extends Component {
    state={
        monthsArr : ['January','February','March','April','May','June','July','August','September','October','November','December'],
        yearsArr:["2018","2019","2020"],
        daysArr:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
        data:{},
        
    };

    async componentDidMount() {
        let {billid} = this.props.match.params;
        let {empuserid} = this.props.user;
        let response = await http.get(`/empapp/hotelbill/${empuserid}/${billid}`);
        let {data} = response;
        this.state.detailsStatus = (Object.values(data).length===10) ?  true:  false;
        if(!data.staystartdate) data.staystartdate = "";
        if(!data.stayenddate) data.stayenddate = "";

        this.setState({data:data});
    }

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        if(input.name.includes("start")) {
            console.log(s1.data.staystartdate);
            s1.data.staystartdate =   this.createDateString(s1.data.staystartdate,input.name,input.value) ;

        }
        else if(input.name.includes("end"))
            s1.data.stayenddate = this.createDateString(s1.data.stayenddate,input.name,input.value);
        else
            s1.data[input.name] = input.value;

        this.setState(s1);
    }

    createDateString=(staydate,name,value)=> {
        console.log(staydate);
        let dateArr = staydate.split("-");
        console.log(dateArr);
        dateArr[0] = (name.includes("Date")) ?  value:dateArr[0] ? dateArr[0]: "";
        
        dateArr[1] = (name.includes("Month")) ?  value  : dateArr[1] ? dateArr[1] :"";
        
        dateArr[2] = (name.includes("Year"))?   value: dateArr[2] ? dateArr[2] : "";
        console.log(dateArr);
       return dateArr.join("-")
    }

   async submitHotelDetails() {
    let response = await http.post('/empapp/hotelbill',this.state.data);
    this.setState({detailsStatus:"submitted"});
    }


    render() {
        let {monthsArr,yearsArr,daysArr,data,detailsStatus} = this.state;
        let {hotel,city,corpbooking,staystartdate="",stayenddate=""} = data;


        return (
            <div className="container text-center">
                <h3>Welcome to Employee Management Portal</h3>
                <h5>Hotel Stay Details</h5>
                Bill id: {this.props.match.params.billid} <br/>
                {detailsStatus ?
                        detailsStatus==='submitted' ?
                     <div className="text-success">Details have been successfully added</div>:
                     <div className="text-success">Displaying Contact Details</div>
                     :<div className="text-danger">No Hotel Stay Details Found. Please Enter Them.</div>  }
                {this.makeDateDropDowns("Check in date:",daysArr,monthsArr,yearsArr,staystartdate,"startDate","startMonth","startYear")}
                {this.makeDateDropDowns("Check out date:",daysArr,monthsArr,yearsArr,stayenddate,"endDate","endMonth","endYear")}
                {this.makeTextBox("Hotel","Enter Hotel name","hotel",hotel)}
                {this.makeTextBox("City","Enter City name","city",city)}
                {this.makeCheckBox("Corporate Booking","corpbooking",corpbooking)}
                <button className="btn btn-primary" disabled={detailsStatus}  onClick={()=>this.submitHotelDetails()}>Submit</button>
            </div>
        )
    }

    makeDateDropDowns = (label,arrDays,arrMonths,arrYears,dateString,name1,name2,name3,error="") => (
        <div className="container w-75">
            <div className="row">
                <div className="col-3">
                    <label>{label}</label> 
                </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name1}
                            value={dateString.split("-")[0]}
                            onChange={this.handleChange}
                        >
                                {arrDays.map((opt) => (<option key={opt}>{opt}</option>))}
                        </select>
                    </div>
                 </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name2}
                            value={dateString.split("-")[1]}
                            onChange={this.handleChange}
                        >
                                {arrMonths.map(opt => (<option  key={opt}>{opt}</option>))}
                        </select>
                    </div>
                </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name3}
                            value={dateString.split("-")[2]}
                            onChange={this.handleChange}
                        >
                                {arrYears.map(opt => (<option  key={opt}>{opt}</option>))}
                        </select>
                    </div>
                </div>
            </div>
                    <div className="d-flex justify-content-center" >
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}
            </div>  
        </div>
    )


    makeCheckBox=(label,name,value)=> (
        <div className="form-check">
            {console.log(value)}
            <input 
                className="form-check-input" 
                type="checkbox" 
                name={name} 
                value={value && value==="Yes" ? "No":"Yes"}
                checked={value==="Yes" ? true :false} 
                onChange={this.handleChange}
            />
            <label className="form-check-label">
                {label}
            </label>
        </div>

    )



    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-3 ">
                    <label>{label}</label>
                </div>
                <div className="col-7">
                    <input
                        type={name==="text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )

   
}
export default HotelBill;