import React, {Component} from "react";
import http from "./services/httpService";
import auth from "./services/authService";
class Login extends Component {
    state={
        form:{},
        errors:{}
    };

    async handleLogin() {
        try {
            let response = await http.post('/empapp/loginuser',this.state.form);
            let {data} = response;
            window.location =  data.role==="ADMIN" ? '/admin':'/emp';
            auth.login(data);
            
         }
         catch(ex) {
            if(ex.response && ex.response.status===401) {
                let s1 = {...this.state};
                s1.errors.login = ex.response.data;
                this.setState(s1);
            }
         }
    }

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget:input} = e;
         s1.form[input.name] = input.value;
         this.setState(s1);
    }


    render() {
        let {errors,form} = this.state;
        let {email,password} = form;
        return (
            <React.Fragment>
                <div className="container text-center bg-light">
                    <h4>Welcome to Employee Management Portal</h4>
                    <h4 className="font-weight-bold">Login</h4>
                {errors.login && <span className="text-danger">{errors.login} Please Check your Username or Password.</span> } 
                    <div class="form-group">
                        <div className="row">
                            <div className="col-5 d-flex justify-content-end">
                                <label>Email ID:</label>
                        </div>
                            <div className="col-4">
                                <input 
                                type="email" 
                                name="email"
                                class="form-control d-inline"  
                                placeholder="Enter email ID"
                                value={email}
                                onChange={this.handleChange}/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div className="row">
                            <div className="col-5 d-flex justify-content-end">
                                <label>Password:</label>
                            </div>
                            <div className="col-4">
                                <input 
                                type="text" 
                                name="password"
                                class="form-control d-inline"  
                                placeholder="Enter password"
                                value={password}
                                onChange={this.handleChange}/>
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-primary btn-sm" onClick={()=>this.handleLogin()}>Submit</button>
                </div>

            </React.Fragment>
        )
    }
}
export default Login;