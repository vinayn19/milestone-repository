import React, {Component} from "react";
import http from "./services/httpService";
import queryString from "query-string";
class Employees extends Component {
    state={
        data:{},
    };

    async componentDidMount() {
        let response = await http.get('/empapp/emps');
        let {data} = response;
        this.setState({data:data});

    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps!==this.props) {
            let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            let response = await http.get(`/empapp/emps${queryStr}`);
            let {data} = response;
            this.setState({data:data});

        }
    }

    async handleEmployeeDetails(id) {
        this.props.history.push(`/admin/viewemp/${id}`);
    }

    navigatePage=(incr)=>{
        let queryParams = queryString.parse(this.props.location.search);
        let {page="1"} = queryParams;
        queryParams.page = +page + incr;
        this.callURL(queryParams);
    }

    callURL=(queryParams)=> {
        let queryStr = this.makeQueryString(queryParams);
        this.props.history.push({
            pathname:'/admin/viewemp',
            search: queryStr,
        })
    }

    makeQueryString=(queryParams)=> {
        let {page="1"} = queryParams;
        let queryStr = "";
         queryStr = this.addToQueryString(queryStr,"page",page);
         return queryStr;
    }

    addToQueryString=(queryStr,queryParam,queryValue)=> {
        if(queryValue)
        queryStr = queryStr + queryStr ? `&${queryParam}=${queryValue}`: `?${queryParam}=${queryValue}`;

        return queryStr;
    }

    render() {
        let { data:emps=[], pageInfo={}} = this.state.data;
        let {pageNumber,numOfItems,numberOfPages,totalItemCount} = pageInfo;
        let initialItem =  numOfItems*pageNumber -(numOfItems-1) ;
        let finalItem = (+initialItem + +numOfItems-1) > numOfItems ? totalItemCount: (+initialItem + +numOfItems-1) ;
        console.log(this.state.data);
        return (
            <React.Fragment>
                <span>{initialItem} to {finalItem} of {totalItemCount}</span>
                <div className="container mt-3 text-center">
                    <div className="row text-white" style={{backgroundColor:"#35586C"}}>
                        <div className="col-4">Name</div>
                        <div className="col-4">Email</div>
                        <div className="col-4"></div>
                    </div>
                {emps.map(emp=> (
                    <div className="row">
                        <div className="col-4 border">{emp.name}</div>
                        <div className="col-4 border">{emp.email}</div>
                        <div className="col-4 border">
                            <button className="btn btn-light btn-sm border border-secondary" onClick={()=>this.handleEmployeeDetails(emp.empuserid)}>Details</button>
                        </div>
                    </div>
                ))}
                <div className="row mt-2">
                    <div className="col-2">
                       {pageNumber > 1 && <button className="btn btn-primary btn-sm"
                       onClick={()=>this.navigatePage(-1)}>Previous</button>} 
                    </div>
                    <div className="col-8"></div>
                    <div className="col-2">
                        {pageNumber < numberOfPages && <button className="btn btn-primary btn-sm"
                        onClick={()=>this.navigatePage(+1)}>Next</button>}
                    </div>
                </div>
                </div>

            </React.Fragment>            
        )
    }
}
export default Employees;