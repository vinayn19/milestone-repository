import React, { Component } from "react";
import {Link} from "react-router-dom";
import auth from "./services/authService";
class Navbar extends Component {

    state={}

    render() {
        let {user} = this.props;
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <Link className="navbar-brand" to={user && user.role===`ADMIN` ? `/admin`:user && user.role===`EMPLOYEE` ? '/emp':'/'} >Employee Portal</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                {user && user.role==="ADMIN" && 
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/admin/addemp">Add Employee</Link>
                        <Link className="dropdown-item" to="/admin/viewemp">View Employees</Link>
                    </div>
                </li>
                </ul>}
                
                {user && user.role==="EMPLOYEE" && 
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        My Portal
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/emp/contact">Contact Details</Link>
                        <Link className="dropdown-item" to="/emp/bills">Bills</Link>
                    </div>
                </li>
                </ul>}
                {!user &&  
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/login`} onClick={this.handleLogin}>LOGIN </Link>
                    </li>
                </ul>}
                {user && 
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/logout`} onClick={this.handleLogout}>LOGOUT </Link>
                    </li>
                </ul>}
            </nav>
        )
    }
}
export default Navbar;