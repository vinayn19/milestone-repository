import React, {Component} from "react";
import http from './services/httpService';

class TravelBill extends Component {
    state={
        monthsArr : ['January','February','March','April','May','June','July','August','September','October','November','December'],
        yearsArr:["2018","2019","2020"],
        daysArr:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
        data:{},
    };

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        if(input.name==="goDate" || input.name==="goMonth" || input.name==="goYear") {
            console.log(s1.data.goflightDate);
            s1.data.goflightDate =   this.createDateString(s1.data.goflightDate,input.name,input.value) ;

        }
        else if(input.name==="backDate" || input.name==="backMonth" || input.name==="backYear")
            s1.data.backflightDate = this.createDateString(s1.data.backflightDate,input.name,input.value);
        else
            s1.data[input.name] = input.value;

        this.setState(s1);
    }

    createDateString=(staydate,name,value)=> {
        console.log(staydate);
        let dateArr = staydate.split("-");
        console.log(dateArr);
        dateArr[0] = (name.includes("Date")) ?  value:dateArr[0] ? dateArr[0]: "";
        
        dateArr[1] = (name.includes("Month")) ?  value  : dateArr[1] ? dateArr[1] :"";
        
        dateArr[2] = (name.includes("Year"))?   value: dateArr[2] ? dateArr[2] : "";
        console.log(dateArr);
       return dateArr.join("-")
    }


    async componentDidMount() {
        let {billid} = this.props.match.params;
        let {empuserid} = this.props.user;
        let response = await http.get(`/empapp/travelbill/${empuserid}/${billid}`);
        let {data} = response;
        this.state.detailsStatus = (Object.values(data).length===14) ? true: false;
        if(!data.goflightDate) data.goflightDate = "";
        if(!data.backflightDate) data.backflightDate = "";
        this.setState({data:data});
    }


    async submitFlightDetails() {
        let response = await http.post('/empapp/travelbill',this.state.data);
        this.setState({detailsStatus:"submitted"});

    }

    render() {
        let {data,detailsStatus,daysArr,monthsArr,yearsArr} = this.state;
        let {goflightDate="",goflightOrigin,goflightDest,goflightNum,corpbooking,backflightDate="",backflightOrigin,backflightDest,backflightNum} = data;
        return (
            <div className="container text-center bg-light" style={{width:"947px"}}>
                <h3>Welcome to Employee Management Portal</h3>
                <h5>Flight Details</h5>
                Bill id: {this.props.match.params.billid}
                {detailsStatus ?
                        detailsStatus==='submitted' ?
                     <div className="text-success">Flight Details have been successfully added</div>:
                     <div className="text-success">Displaying Flight Details</div>
                     :<div className="text-danger">No Flight Details Found. Please Enter Them.</div>  }

                     <div className="container border-top border-bottom py-2">
                         <div className="font-weight-bold py-4">Departure Flight Details</div>
                        {this.makeDateDropDowns("Flight Date:",daysArr,monthsArr,yearsArr,goflightDate,"goDate","goMonth","goYear")}
                        {this.makeTextBox("Origin City:","Enter Flight Origin City","goflightOrigin",goflightOrigin)}
                        {this.makeTextBox("Destination City:","Enter Flight Destination City","goflightDest",goflightDest)}
                        {this.makeTextBox("Flight Number:","Enter Flight Number","goflightNum",goflightNum)}

                     </div>
                     <div className="container border-top py-2">
                         <div className="font-weight-bold py-4">Return Flight Details</div>
                        {this.makeDateDropDowns("Flight Date:",daysArr,monthsArr,yearsArr,backflightDate,"backDate","backMonth","backYear")}
                        {this.makeTextBox("Origin City:","Enter Flight Origin City","backflightOrigin",backflightOrigin)}
                        {this.makeTextBox("Destination City:","Enter Flight Destination City","backflightDest",backflightDest)}
                        {this.makeTextBox("Flight Number:","Enter Flight Number","backflightNum",backflightNum)}
                        {this.makeCheckBox("Corporate Booking","corpbooking",corpbooking)}
                     </div>
                <button className="btn btn-primary" disabled={detailsStatus}  onClick={()=>this.submitHotelDetails()} onClick={()=>this.submitFlightDetails()}>Submit</button>

            </div>
        )
    }

    makeCheckBox=(label,name,value)=> (
        <div className="form-check">
            {console.log(value)}
            <input 
                className="form-check-input" 
                type="checkbox" 
                name={name} 
                value={value && value==="Yes" ? "No":"Yes"}
                checked={value==="Yes" ? true :false} 
                onChange={this.handleChange}
            />
            <label className="form-check-label">
                {label}
            </label>
        </div>

    )

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-3 ">
                    <label>{label}</label>
                </div>
                <div className="col-7">
                    <input
                        type={name==="text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    )

    makeDateDropDowns = (label,arrDays,arrMonths,arrYears,dateString,name1,name2,name3,error="") => (
        <div className="container w-75">
            <div className="row">
                <div className="col-3">
                    <label>{label}</label> 
                </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name1}
                            value={dateString.split("-")[0]}
                            onChange={this.handleChange}
                        >
                                {arrDays.map((opt) => (<option key={opt}>{opt}</option>))}
                        </select>
                    </div>
                 </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name2}
                            value={dateString.split("-")[1]}
                            onChange={this.handleChange}
                        >
                                {arrMonths.map(opt => (<option  key={opt}>{opt}</option>))}
                        </select>
                    </div>
                </div>
                <div className="col-3">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name3}
                            value={dateString.split("-")[2]}
                            onChange={this.handleChange}
                        >
                                {arrYears.map(opt => (<option  key={opt}>{opt}</option>))}
                        </select>
                    </div>
                </div>
            </div>
                    <div className="d-flex justify-content-center" >
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}
            </div>  
        </div>
    )


   
}
export default TravelBill;