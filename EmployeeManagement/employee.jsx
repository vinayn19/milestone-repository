import React, {Component} from "react";
import auth from "./services/authService";
class Employee extends Component {
    state={};


    render() {
        let emp = this.props.user;
        return (
            <React.Fragment>
                <h3>Welcome {emp.name} to the Employee Management Portal</h3>
            </React.Fragment>
        )
    }
}
export default Employee;