import React, {Component} from "react";
import http from "./services/httpService";
class EmpDepartmentDetails extends Component {
    state={
        data:{},
        errors:{},
        formDetails:{},
    };


    async componentDidMount() {
        let {empuserid} = this.props.match.params;
        let response = await http.get(`/empapp/empdept/${empuserid}`);
        let {data:formDetails} = response;
        if(Object.keys(formDetails).length===4) {
            this.state.detailsStatus = true;
        } else {
            this.state.detailsStatus = false;
        }
        
        this.setState({formDetails:formDetails});    

    }

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        s1.errors[input.name] = this.makeFormErrors(input.name,input.value);
        s1.formDetails[input.name] = input.value;
        this.setState(s1);
    }

    makeFormErrors=(name,value)=> {
        switch(name) {
            case "department":
                return value 
                ? ""
                :"Department is Mandatory";
            case "designation":
                return value 
                ? ""
                :"Designation is Mandatory";
            case "manager":
                return value 
                ? ""
                :"Manager's name is Mandatory";
        }
    }



    async handleSubmit(id) {
        let response = await http.post(`/empapp/empdept/${id}`,this.state.formDetails);
        this.setState({detailsStatus:"submitted"});
    }
 
    render() {
        let {formDetails,detailsStatus,errors={}} = this.state;
        let {department,designation,manager,empuserid} = formDetails;     
        return (
            <div className="container text-center bg-light">
                  <h3>Welcome to Employee Management Portal</h3>          
                    <h4>Department Details of New Employee</h4>
                    {detailsStatus ?
                        detailsStatus==='submitted' ?
                     <div className="text-success">Details have been successfully added</div>:
                     <div className="text-success">Displaying Department Details</div>
                     :<div className="text-danger">No Department Details Found. Please Enter Them.</div>  }
                    {this.makeTextBox("Department","Enter the Employee's Department","department",department,errors.department)}
                    {this.makeTextBox("Designation","Enter the Employee's Designation","designation",designation,errors.designation)}
                    {this.makeTextBox("Manager's Name","Enter the Manager's Name","manager",manager,errors.manager)}
                   <button className="btn btn-primary py-1 font-weight-bold rounded-0" disabled={detailsStatus  } onClick={()=>this.handleSubmit(empuserid)}>Submit</button>
                    
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row d-flex justify-content-center">
                <div className="col-4">
                    <label>{label}</label>
                </div>
                <div className="col-6">
                    <input
                        type={name==="text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
                </div>
            </div>
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default EmpDepartmentDetails;