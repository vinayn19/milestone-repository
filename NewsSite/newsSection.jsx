import React, {Component} from "react";
import queryString from "query-string";
import http from "./httpService";
import LeftPanelRadios from "./leftPanelRadios";
class NewsSection extends Component {
    state={
        data:{},
        search:""
  
    };



    async componentDidUpdate(prevProps,prevState){
        if(prevProps!=this.props) {
            let queryParams = queryString.parse(this.props.location.search);
            let {q,page,section="",orderBy} = queryParams;
            this.state.search = q;
            let apiUrl = `api-key=test`;
             apiUrl = this.makeApiUrl(apiUrl,"page",page);
             apiUrl = this.makeApiUrl(apiUrl,"q",q);
             apiUrl = this.makeApiUrl(apiUrl,"section",section);
             apiUrl = this.makeApiUrl(apiUrl,"order-by",orderBy);
            apiUrl = '?' + apiUrl;
            let info =  await http.get(apiUrl);
              let response = info.data.response;
            this.setState({data:response}); 
            
        }
    }

    makeApiUrl=(apiUrl,paramName,paramValue)=> {
        if(paramValue) 
        return   `${paramName}=${paramValue}&` + apiUrl;
        else return apiUrl;
    }
    

    handleChangeOptions=(options)=> {
        this.state.orderBy = options.orderBy;
        this.callURL(options);
    }

    callURL=(options)=> {
        let queryString = this.makeQueryString(options);
        this.props.history.push({
            pathname: '/home',
            search: queryString,
        })
    }

    makeQueryString=(options)=> {
        let {q, page="1",section,orderBy} = options;
        console.log("options",options);
        let queryString="";
        queryString = this.addToQueryString(queryString,"q",q);
        queryString = this.addToQueryString(queryString,"page",page);
        queryString = this.addToQueryString(queryString,"section",section);
        queryString = this.addToQueryString(queryString,"orderBy",orderBy);
        return queryString;
    }

    addToQueryString=(queryString,paramName,paramValue)=> {
        if(paramValue)
           return queryString ?
                queryString + `&${paramName}=${paramValue}`:
                queryString + `?${paramName}=${paramValue}`; 
        else return queryString;
    }

    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {q,page:pageInitial} = queryParams;
        let page = +pageInitial + +incr;
        queryParams.page = page ;
        this.callURL(queryParams);
        
    }
    submitSearch=()=> {
        let s1 = {...this.state};
        let queryParams = queryString.parse(this.props.location.search);
        queryParams.q = s1.search;
        this.callURL(queryParams);
    }

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget:input} = e;
        s1[input.name] = input.value;
        this.setState(s1);
    }

    render() {
        let queryParams = queryString.parse(this.props.location.search);
        console.log("inside render",this.props.location.search);
        console.log("inside render",queryParams);
        let {q,page} = queryParams;
        let {pages,results=[],startIndex,total,pageSize,orderBy} = this.state.data;
        let {search} = this.state;
    
        
        return (
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <LeftPanelRadios options={queryParams} handleChangeOptions={this.handleChangeOptions}/>

                    </div>
                    <div className="col-9">
                        {this.makeTextBox("Search","Enter search text","search",search)}

                        {q? (
                            <React.Fragment>
                                {total ? (`${startIndex}-${ startIndex + pageSize -1}/${total}`):""}  
                                    <div className="row text-center" >
                                    {results.map(news=> (
                                            <div className="col-3 m-1 border" style={{backgroundColor:"#C1D8F0"}} key={news.id}>
                                                {news.webTitle} <br/>
                                            <div className="font-weight-bold"> Source: {news.sectionName}</div>
                                            <div><a href={news.webUrl}>Preview</a> </div>
                                            </div>
                                            
                                    ))}
                                    </div>
                                <div className="row mt-2">
                                    <div className="col-2">
                                    {page >1 ?  <button className="btn btn-info" onClick={()=>this.changePage(-1)}>Previous</button> :""}
                                    </div>
                                    <div className="col-8"></div>
                                    <div className="col-2">
                                    {page < pages ? <button className="btn btn-info" onClick={()=>this.changePage(+1)}>Next</button> :""}
                                    </div>
                                </div>

                            </React.Fragment>
                        )
                        :""}

                    </div>
                </div>
            </div>
            )

    }


    makeTextBox = (label, placeholder, name,value="") => (
        <div className="form-group">
            {console.log(value)}
             <label className="m-0 p-3 font-weight-bold">{label}</label>
             <div className="row">
                 <div className="col-10">
                    <input
                        type={name==="price" ?"number": "text"}
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />

                 </div>
                 <div className="col-2">
                     <button className="btn btn-danger" onClick={this.submitSearch}>Submit</button>
                 </div>
             </div>
        </div>
    
    )

}
export default NewsSection;