import React, {Component} from "react";
import {Switch,Route,Redirect} from "react-router-dom";
import Navbar from "./navbar";
import NewsSection from "./newsSection";
class MainComponent extends Component {
    state={};


    render() {
        return (
            <div className="container-fluid p-0">
            <Navbar/>
                      
                <Switch>
                    <Route path={"/home"} component={NewsSection}/>
                </Switch>

            </div>
        )
    }
    
}
export default MainComponent;