import React, {Component} from "react";
import {Link} from "react-router-dom";
class Navbar extends Component {
    state={
        navSelect:-1,
    };

    selectedNavbar=(navNum)=> {
        this.setState({ navSelect:navNum});

    }


    render() {
        let {navSelect} = this.state;
        let active1="",active2="",active3="",active4="";
         switch (navSelect) {
            case 0:
                active1 = "active";  
                break;
            case 1:
                active2 = "active";  
                break;
            case 2:
                active3 = "active";  
                break;
            case 3:
                active4 = "active";  
                break;                
            default:
                break;
        }


        return (
            <nav className="navbar navbar-expand-sm navbar-dark" style={{backgroundColor:"#f88379"}}>
                <Link className="navbar-brand " to={`/home`} style={{color:"#a94064"}}>NewsSite</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className={"nav-link " + active1 } to={`/home?q=sports&page=1`}
                            onClick={()=>this.selectedNavbar(0)} >Sports</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active2 } to={`/home?q=cricket&page=1`}
                            onClick={()=>this.selectedNavbar(1)}>Cricket</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active3 } to={`/home?q=movies&page=1`}
                            onClick={()=>this.selectedNavbar(2)}>Movies</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active4 } to={`/home?q=education&page=1`}
                            onClick={()=>this.selectedNavbar(3)}>Education</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Navbar;