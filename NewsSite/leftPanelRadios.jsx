import React, {Component} from "react";
class LeftPanelRadios extends Component {
    state={
        orderByArr:["none","newest","oldest","relevance"],
        sectionsArr:["Business","Technology","Politics","Lifestyle"],

    };

    handleChange=(e)=> {
        let {options,handleChangeOptions} = this.props;
        let {currentTarget: input} = e;
        options[input.name] = input.value;

        handleChangeOptions(options);
    }


    render() {
        let {orderByArr,sectionsArr} = this.state;
        let {q,page,section,orderBy} = this.props.options;
        
        return (
            <React.Fragment>
                {this.makeDropDowns("Order By",orderByArr,"orderBy",orderBy)}
                {this.makeRadios("Sections",sectionsArr,"section",section)}
            </React.Fragment>
        )
    }

    makeDropDowns = (label,arr,name,value='newest') => (
        <div className="form-group">
            <label className="m-0 p-3 font-weight-bold">{label}</label>
            <select
                className="form-control"
                name={name}
                value={value}
                onChange={this.handleChange}
            >
                    <option value="" disabled={true}> {label} </option>
                    {arr.map(opt => ( 
                        
                         <option key={opt} >{opt}</option>
                    ))}
    
            </select>
        </div>
    )
    
    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid border px-0 mt-4">
            <label className="m-0 p-3 pl-4 font-weight-bold">{label}</label>
            {arr.map(opt=> (
            <div className="form-check p-3 border">
                <div className="pl-4">
                <input 
                className="form-check-input" 
                type="radio" 
                name={name} 
                value={opt.toLowerCase()}
                checked={value.toLowerCase()===opt.toLowerCase()}
                onChange={this.handleChange}
                />
                <label className="form-check-label">
                    {opt}
                </label>

                </div>
            </div>
    
            ))}
                
    
        </div>
    )
    
}
export default LeftPanelRadios;