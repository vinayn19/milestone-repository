import React, { Component } from 'react';
import http from "./httpAxiosService";
import {Button} from "@material-ui/core";

class FinalPage extends Component {
    state = { player:"", scores:[], showScores:false }

    async componentDidMount() {
        let id = this.props.match.params.id;
        let response = await http.get(`/getPlayer/${id}`) 
        this.setState({player: response.data});
    }

    showScoreBoard= async ()=> {
        let response = await http.get('/scores');
        let {data} = response;
        this.setState({scores:data, showScores:!this.state.showScores})
    }

    handlePlayAgain=()=> {
        let id = this.props.match.params.id;
        this.props.history.push(`/playGround/${id}`);
    }

    gotoHomePage=()=> {
        this.props.history.push('/');
    }

    render() { 
        let {player, scores,showScores} = this.state;
        let {name,score} = player;
        return ( 
        <div className="container text-center mt-5">
            <h3>Game Over</h3>
           <div className="text-success font-weight-bold">
               You played awsom {name}! <br />
               Your Score : {score}/5
           </div>  
            <div className="row d-flex justify-content-around mt-3">
                <div className="col-4">
                    <Button variant="contained" color="primary" onClick={this.playAgain} onClick={this.handlePlayAgain} >
                        Play Again
                    </Button>
                </div>
                <div className="col-4">
                    <Button variant="contained" color="primary" onClick={this.gotoHomePage}>
                        Home Page
                    </Button>
                </div>
                <div className="col-4">
                    <Button variant="contained" color="primary" onClick={this.showScoreBoard}>
                        Leaderboards
                    </Button>
                </div>
            {showScores && <table className="table mt-3 table-hover">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Timestamp</th>
                        <th scope="col">Score</th>
                    </tr>
                </thead>
                <tbody>
                {scores.map(score=> (
                  <tr>
                      <th scope="col">{score.name}</th>
                      <th scope="col">{score.time}</th>
                      <th scope="col">{score.score}</th>
                  </tr>
                ))}
                </tbody>
            </table>}
            </div>
        </div> 
        );
    }
}
 
export default FinalPage;