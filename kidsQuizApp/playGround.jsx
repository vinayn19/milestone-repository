import React, { Component } from "react";
import http from "./httpAxiosService";
import { Button } from "@material-ui/core";
import "./styles.css";

class PlayGround extends Component {
	state = {
		questions: [{ image: "", options: [] }],
		quesIndex: 0,
		selectedAnswer: null,
		attempts: 0,
		playerInfo: {},
	};

	async componentDidMount() {
		let id = this.props.match.params.id;
		let response1 = await http.get("/getQuestions");
		let response2 = await http.get(`/getPlayer/${id}`);
		response2.data.score = 0;
		this.setState({
			questions: response1.data,
			quesIndex: 0,
			playerInfo: response2.data,
		});
	}

	checkOption = (option) => {
		let s1 = { ...this.state };
		let id = this.props.match.params.id;
		let question = s1.questions[s1.quesIndex];
		s1.selectedAnswer = option;
		s1.attempts++;
		if (option === question.correctAnswer) {
			if (s1.attempts === 1) s1.playerInfo.score++;
			setTimeout(async () => {
				s1.quesIndex++;
				s1.selectedAnswer = null;
				s1.attempts = 0;
				if (s1.quesIndex < 5) this.setState(s1);
				else {
					try {
						await http.put("/updatePlayerScore", this.state.playerInfo);
						this.props.history.push(`/finalPage/${id}`);
					} catch (ex) {
						console.log(ex);
					}
				}
			}, 3000);
		}

		this.setState(s1);
	};

	render() {
		const { questions, quesIndex, selectedAnswer, playerInfo } = this.state;
		let currentQues = questions[quesIndex];
		let { image, options, correctAnswer } = currentQues;
		let title =
			selectedAnswer === correctAnswer ? (
				<span className="text-success">Congrats !🤩🎉 Right answer </span>
			) : selectedAnswer === null ? (
				"Identify the Animal"
			) : (
				<span className="text-danger">Oops !🙁 Try Again..</span>
			);

		return (
			<div className="container text-center">
				<div className="container mt-3 " style={{ width: "50%" }}>
					<h3>Welcome {playerInfo.name} </h3>
					<h4 className="font-weight-bold">{title}</h4>
					<img
						src={image}
						className="img-fluid"
						alt=""
						style={{
							boxShadow: "3px 3px 3px 3px ",
							overflow: "hidden",
							borderRadius: "7px",
							maxHeight: "295px",
							minHeight: "200px",
						}}
					/>
				</div>
				<div className="my-3 font-weight-bold">
					Score: {playerInfo.score}
				</div>
				<div className="row mx-auto my-3">
					{options.map((opt) => (
						<div className="col-md-3 col-sm-6 col-xs-12 mt-3 d-flex justify-content-around">
							<Button
								onClick={() => this.checkOption(opt)}
								variant="contained"
								style={{
									backgroundColor:
										selectedAnswer === opt
											? selectedAnswer === correctAnswer
												? "green"
												: "red"
											: "#8bc34a",
									width: "50%",
									height: "50%",
									padding: "25px 30px",
								}}
							>
								{opt}
							</Button>
						</div>
					))}
				</div>
			</div>
		);
	}
}

export default PlayGround;
