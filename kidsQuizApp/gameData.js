
let questions = [
    { id: 1, image: "https://i.ibb.co/X8NSQY6/wolf.jpg", correctAnswer: "Fox", options: ["Fox", "Sheep", "Lion", "Pigeon"] },
    { id: 2, image: "https://i.ibb.co/DKKd41T/vulture.jpg", correctAnswer: "Vulture", options: ["Turtle", "Pigeon", "Vulture", "Peacock"] },
    { id: 3, image: "https://i.ibb.co/8B5jzMs/turtle.jpg", correctAnswer: "Turtle", options: ["Rabbit", "Turtle", "Lion", "Pigeon"] },
    { id: 4, image: "https://i.ibb.co/cFz8Nnh/squirrel.jpg", correctAnswer: "Squirrel", options: ["Rat", "Sheep", "Squirrel", "Pigeon"] },
    { id: 5, image: "https://i.ibb.co/vc6wFsB/shark.jpg", correctAnswer: "Shark", options: ["Camel", "Sheep", "Lion", "Shark"] },
    { id: 6, image: "https://i.ibb.co/pRkQFrt/rabbit.jpg", correctAnswer: "Rabbit", options: ["Racoon", "Rabbit", "Lion", "Pigeon"] },
    { id: 7, image: "https://i.ibb.co/tPWyCSZ/peacock.jpg", correctAnswer: "Peacock", options: ["Peacock", "Pegion", "Squirrel", "Crow"] },
    { id: 8, image: "https://i.ibb.co/4mvBtzn/panda.jpg", correctAnswer: "Panda", options: ["Fox", "Panda", "Lion", "Pigeon"] },
    { id: 9, image: "https://i.ibb.co/p0SJJQ4/Lion.jpg", correctAnswer: "Lion", options: ["Fox", "Sheep", "Tiger", "Lion"] },
    { id: 10, image: "https://i.ibb.co/zfWnR3Y/Leopard.jpg", correctAnswer: "Leopard", options: ["Fox", "Ant eater", "Leopard", "Tiger"] },
    { id: 11, image: "https://i.ibb.co/PzQBKZn/kangroo.jpg", correctAnswer: "Kangaroo", options: ["Fox", "Kangaroo", "Lion", "Sloth"] },
    { id: 12, image: "https://i.ibb.co/2PxbyZS/horse.jpg", correctAnswer: "Horse", options: ["Elephant", "Sheep", "Lion", "Horse"] },
    { id: 13, image: "https://i.ibb.co/Y3SPjRw/hen.jpg", correctAnswer: "Hen", options: ["Crow", "Hen", "Eagle", "Pigeon"] },
    { id: 14, image: "https://i.ibb.co/5rqrHL4/Hamster.jpg", correctAnswer: "Hamster", options: ["Rabbit", "Rat", "Lion", "Hamster"] },
    { id: 15, image: "https://i.ibb.co/DwgtDFX/gekko.jpg", correctAnswer: "Gekko", options: ["Fox", "Gekko", "Rat", "Pigeon"] },
    { id: 16, image: "https://i.ibb.co/RpqgJZV/frog.jpg", correctAnswer: "Frog", options: ["Frog", "Sheep", "Lion", "Pigeon"] },
    { id: 17, image: "https://i.ibb.co/z6GGgfs/baby-Monkey.jpg", correctAnswer: "Baby Monkey", options: ["Fox", "Sheep", "Baby Monkey", "Pigeon"] },
    { id: 18, image: "https://i.ibb.co/J3Xt7kk/eagle.jpg", correctAnswer: "Eagle", options: ["Fox", "Sheep", "Eagle", "Pigeon"] },
    { id: 19, image: "https://i.ibb.co/r74140c/deer.jpg", correctAnswer: "Deer", options: ["Fox", "Sheep", "Lion", "Deer"] },
    { id: 20, image: "https://i.ibb.co/2PCg5b6/butterfly.jpg", correctAnswer: "Butterfly", options: ["Fox", "Butterfly", "Lion", "Pigeon"] },
]

let scores = [
    { id: 1, name: "Rohan", time: "25/05/2021 8:45 AM", score: 3 },
    { id: 2, name: "Vishal", time: "26/05/2021 7:53 PM", score: 4 }
]


module.exports = { questions: questions, scores: scores }