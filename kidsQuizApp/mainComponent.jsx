import React, { Component } from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import WelcomePage from "./welcomePage";
import PlayGround from "./playGround";
import FinalPage from "./finalPage";

class MainComponent extends Component {
    state = {}


    render() { 
        let {playerName} = this.state;
        return (
             <Switch>
                <Route path='/Home' render={(props)=><WelcomePage {...props} submitName={this.handleSubmitName} /> }/>
                <Route path='/playGround/:id' render={(props)=><PlayGround {...props} /> }/>
                <Route path='/finalPage/:id' render={(props)=><FinalPage {...props} /> }/>
                <Redirect from='/' to='/Home'/>
             </Switch> 
        );
    }
}
 
export default MainComponent;