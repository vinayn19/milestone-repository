let express = require("express");
const timestamp = require('time-stamp');

let app = express();

let questions = require('./gameData').questions;
let scores = require('./gameData').scores;
let port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Listening on http://localhost:${port}`));

app.use(express.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
        );
        res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
        next();
    });
    
    app.get('/getQuestions', (req, res) => {
        const shuffled = questions.sort(() => 0.5 - Math.random());
        const numberOfQuestions = 5;
        let selectedQuestions = shuffled.slice(0, numberOfQuestions);
        res.send(selectedQuestions)
    })
    
    app.post('/player', (req, res) => {
    var time = new Date();
    let date_ob = new Date();

    const playerName = req.body.name;
    const timeStamp =  timestamp('DD/MM/YYYY') +" " +time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
   
    let scoreBoard = { id: scores.length + 1,name:playerName, time: timeStamp, score: 0 };
    scores.push(scoreBoard);
    res.send(scoreBoard);
})

app.get('/scores', (req, res) => {
    res.send(scores);
})


app.get('/getPlayer/:id', (req, res) => {
    let id = +req.params.id;
    let scoreBoard =  scores.find(score=> score.id === id);
    res.send(scoreBoard);
})

app.put('/updatePlayerScore', (req, res) => {
    let {id,score : currentScore}  = req.body;
    let scoreBoardIndex =  scores.findIndex(score=> score.id === id);
    scores[scoreBoardIndex].score = currentScore; 
    res.send({...req.body});
})