import React, { Component } from 'react';
import { TextField } from '@material-ui/core';
import { Button } from '@material-ui/core';
import http from "./httpAxiosService";
import './styles.css';
class WelcomePage extends Component {
    state = { name:"" }
    
            handleChange=(e)=> {
                let s1 = {...this.state};
                let {currentTarget: input} = e;
                s1[input.name] = input.value;
                this.setState(s1);
            }

        submitName= async ()=> {
            try {
                 let response = await http.post('/player',{name:this.state.name});
                    let {data } = response; 
                 this.props.history.push(`/playGround/${data.id}`);
            }catch(ex) {
                console.log(ex);
            }
        }


    render() { 
            let {name} = this.state;
        return ( 
            <div className="container mx-auto mt-5 " style={{width:'40%'}}>
                <h3>Welcome to Guess that Animal 🐼</h3> 
                <form noValidate autoComplete="off">
                    <TextField id="standard-basic" label="Player's Name"  name="name" value={name}  style={{width:'70%'}} onChange={this.handleChange} /> 
                    <div className="mt-2">
                    <Button variant="contained" color="primary" onClick={()=>this.submitName(this.state.name)}>
                         Submit
                    </Button>
                    </div>
                </form>
            </div>
         );
    }
}
 
export default WelcomePage;