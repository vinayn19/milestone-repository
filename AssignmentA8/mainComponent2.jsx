import React, { Component } from "react";
class MyComp extends Component {
    state = {
        sizes: ["Regular", "Medium", "Large"],
        crusts: ["New Hand Tossed", "Wheat Thin Crust", "Cheese Burst", "Fresh Pan Pizza", "Classic HandTossed"],
        items: [
            {
                "id": "MIR101", "image": "https://i.ibb.co/SR1Jzpv/mirinda.png", "type": "Beverage", "name": "Mirinda", "desc": "Mirinda", "veg": "Yes"
            },
            {
                "id": "PEP001", "image": "https://i.ibb.co/3vkKqsF/pepsiblack.png", "type": "Beverage", "name": "Pepsi Black Can", "desc": "Pepsi Black Can", "veg": "Yes"
            },
            {
                "id": "LIT281", "image": "https://i.ibb.co/27PvTng/lit.png", "type": "Beverage", "name": "Lipton IcedTea", "desc": "Lipton Iced Tea", "veg": "Yes"
            },
            {
                "id": "PEP022", "image": "https://i.ibb.co/1M9xDZB/pepsi-new-20190312.png", "type": "Beverage", "name": "Pepsi New", "desc": "Pepsi New", "veg": "Yes"
            },
            {
                "id": "BPCNV1", "image": "https://i.ibb.co/R0VSJjq/Burger-Pizza-Non-Veg-nvg.png", "type": "Burger Pizza", "name": "Classic Non Veg", "desc": "Oven - baked buns with cheese, peri- peri chicken, tomato &capsicum in creamy mayo", "veg": "No"
            },
            {
                "id": "BPCV03", "image": "https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel-1.png", "type": "Burger Pizza", "name": "Classic Veg", "desc": "Oven - baked buns with cheese, tomato &capsicum in creamy mayo", "veg": "Yes"
            },
            {
                "id": "BPPV04", "image": "https://i.ibb.co/Xtx43fT/Burger-Pizza-Veg-423-X420-Pixel-1.png", "type": "Burger Pizza", "name": "Premium Veg", "desc": "Oven - baked buns with cheese, paneer,tomato, capsicum & red paprika in creamy mayo", "veg": "Yes"
            },
            {
                "id": "DIP033", "image": "https://i.ibb.co/0mbBzsw/new-cheesy.png", "type": "SideDish", "name": "Cheesy Dip", "desc": "An all- time favorite with your Garlic Breadsticks & Stuffed GarlicBread for a Cheesy indulgence", "veg": "Yes"
            },
            {
                "id": "DIP072", "image": "https://i.ibb.co/fY52zBw/new-jalapeno.png", "type": "SideDish", "name": "Cheesy Jalapeno Dip", "desc": "A spicy, tangy flavored cheese dip is a an absolutedelight with your favourite Garlic Breadsticks", "veg": "Yes"
            },
            {
                "id": "GAR952", "image": "https://i.ibb.co/BNVmfY9/Garlic-bread.png", "type": "SideDish", "name": "Garlic Breadsticks", "desc": "Baked to perfection.Your perfect pizza partner! Tastesbest with dip", "veg": "Yes"
            },
            {
                "id": "PARCH1", "image": "https://i.ibb.co/prBs3NJ/Parcel-Nonveg.png", "type": "SideDish", "name": "Chicken Parcel", "desc": "Snacky bites! Pizza rolls with chicken sausage & creamyharissa sauce", "veg": "No"
            },
            {
                "id": "PARVG7", "image": "https://i.ibb.co/JHhrM7d/Parcel-Veg.png", "type": "Side Dish", "name": "VegParcel", "desc": "Snacky bites! Pizza rolls with paneer & creamy harissa sauce", "veg": "Yes"
            },
            {
                "id": "PATNV7", "image": "https://i.ibb.co/0m89Jw9/White-Pasta-Nvg.png", "type": "SideDish", "name": "White Pasta Italiano Non- Veg", "desc": "Creamy white pasta with pepper barbecuechicken", "veg": "No"
            },
            {
                "id": "PATVG4", "image": "https://i.ibb.co/mv8RFbk/White-Pasta-Veg.png", "type": "SideDish", "name": "White Pasta Italiano Veg", "desc": "Creamy white pasta with herb grilledmushrooms", "veg": "Yes"
            },
            {
                "id": "DES044", "image": "https://i.ibb.co/gvpDKPv/Butterscotch.png", "type": "Dessert", "name": "Butterscotch Mousse Cake", "desc": "Sweet temptation! Butterscotch flavored mousse", "veg": "Yes"
            },
            {
                "id": "DES028", "image": "https://i.ibb.co/nm96NZW/Choco-Lava.png", "type": "Dessert", "name": "Choco Lava Cake", "desc": "Chocolate lovers delight! Indulgent,gooey molten lava inside chocolate cake", "veg": "Yes"
            },
            {
                "id": "PIZVDV", "image": "https://i.ibb.co/F0H0SWG/deluxeveg.png", "type": "Pizza", "name": "DeluxeVeggie", "desc": "Veg delight - onion, capsicum, grilled mushroom, corn & paneer", "veg": "Yes"
            },
            {
                "id": "PIZVFH", "image": "https://i.ibb.co/4mHxB5x/farmhouse.png", "type": "Pizza", "name": "Farmhouse", "desc": "Delightful combination of onion, capsicum, tomato & grilled mushroom", "veg": "Yes"
            },
            {
                "id": "PIZVIT", "image": "https://i.ibb.co/sRH7Qzf/Indian-Tandoori-Paneer.png", "type": "Pizza", "name": "Indi Tandoori Paneer", "desc": "It is hot.It is spicy.It is oh- so -Indian.Tandoori paneer with capsicum, red paprika & mint mayo", "veg": "Yes"
            },
            {
                "id": "PIZVMG", "image": "https://i.ibb.co/MGcHnDZ/mexgreen.png", "type": "Pizza", "name": "Mexican Green Wave", "desc": "Mexican herbs sprinkled on onion, capsicum, tomato & jalapeno", "veg": "Yes"
            },
            {
                "id": "PIZVPP", "image": "https://i.ibb.co/cb5vLX9/peppypaneer.png", "type": "Pizza", "name": "PeppyPaneer", "desc": "Flavorful trio of juicy paneer, crisp capsicum with spicy red paprika", "veg": "Yes"
            },
            {
                "id": "PIZVVE", "image": "https://i.ibb.co/gTy5DTK/vegextra.png", "type": "Pizza", "name": "VegExtravaganza", "desc": "Black olives, capsicum, onion, grilled mushroom, corn, tomato, jalapeno &extra cheese", "veg": "Yes"
            },
            {
                "id": "PIZNCP", "image": "https://i.ibb.co/b5qBJ9d/cheesepepperoni.png", "type": "Pizza", "name": "Chicken Pepperoni", "desc": "A classic American taste! Relish the delectable flavor of Chicken Pepperoni,topped with extra cheese", "veg": "No"
            },
            {
                "id": "PIZNCD", "image": "https://i.ibb.co/GFtkbB1/Chicken-Dominator10.png", "type": "Pizza", "name": "Chicken Dominator", "desc": "Loaded with double pepperbarbecue chicken, peri- peri chicken, chicken tikka & grilled chicken rashers", "veg": "No"
            },
            {
                "id": "PIZNPB", "image": "https://i.ibb.co/GxbtcLK/Pepper-Barbeque-Onion-C.png", "type": "Pizza", "name": "Pepper Barbecue & Onion", "desc": "A classic favourite with pepperbarbeque chicken & onion", "veg": "No"
            },
            {
                "id": "PIZNIC", "image": "https://i.ibb.co/6Z5wBqr/Indian-Tandoori-Chicken-Tikka.png", "type": "Pizza", "name": "Indi Chicken Tikka", "desc": "The wholesome flavour of tandoorimasala with Chicken tikka, onion, red paprika & mint mayo", "veg": "No"
            }
        ],
        view: 0,
        cart: [],
        selectedPizzas: [],
    };
    

    handleChange = (e, pizzaId) => {
        let s1 = { ...this.state };
        let { currentTarget: input } = e;
        let currentPizza = { "idPizza": pizzaId };
        currentPizza[input.name] = input.value;
        if (s1.selectedPizzas.length === 0) {
            s1.selectedPizzas.push(currentPizza);
        } else {
            let pizzaIndex = s1.selectedPizzas.findIndex(it => it.idPizza === pizzaId);
            if (pizzaIndex === -1) {
                s1.selectedPizzas.push(currentPizza);

            } else {
                s1.selectedPizzas[pizzaIndex][input.name] = input.value;
            }
        }

        this.setState(s1);
    }

    addToCart = (pizzaId, pizza) => {
        let s1 = { ...this.state };
        console.log("In Add to CArt");
        let pizzaAdded = s1.selectedPizzas.find(it => it.idPizza === pizzaId);
        if (s1.view === 0 || s1.view === 1) {
          
            if (typeof pizzaAdded === 'undefined') alert("Please choose the size");
            else if (pizzaAdded) {
                if(typeof pizzaAdded.size==='undefined') 
                    alert("Please choose the size");
                else if (typeof pizzaAdded.crust === 'undefined')
                    alert("Please choose the crust");
                else {
                        s1.cart.push({ ...pizza, "size": pizzaAdded.size, "crust": pizzaAdded.crust, "quantity": 1 });
                        console.log(s1.cart);
                        this.setState(s1);
        
                    }
                
            }
        } else {
            s1.cart.push({ ...pizza, "quantity": 1 });
            console.log(s1.cart);
            this.setState(s1);

        }
        this.setState(s1);
    }

    showItems = (items) => {
        let { sizes, crusts, cart, view,selectedPizzas } = this.state;
        return (
            <React.Fragment>
                <div className="container-fluid  border">
                    <div className="container-fluid float-left border my-4" style={{ width: "60%" }}>
                        <div className="row">
                            {items.map((pizza, index) => (
                                <div className="col-6 text-center">
                                    <div className="card">
                                        <img src={pizza.image} className="card-img-top"></img>
                                        <div className="card-body p-3">
                                            <h6 className="card-title">{pizza.name}</h6>
                                            <p className="card-text">{pizza.desc}</p>
                                            <div className="d-flex justify-content-between" id={pizza.id}>
                                                <React.Fragment>
                                                    {this.makeDropDowns("Select Size", sizes, pizza.type === "Pizza" ? "sizes" : "d-none", pizza, cart, "size",
                                                        selectedPizzas.findIndex(it => it.idPizza === pizza.id) >= 0 ? selectedPizzas.findIndex(it => it.idPizza === pizza.id).size:"" )}
                                                    {this.makeDropDowns("Select Crust", crusts, pizza.type === "Pizza" ? "crusts" : "d-none", pizza, cart, "crust",
                                                        selectedPizzas.findIndex(it => it.idPizza === pizza.id) >= 0 ? selectedPizzas.findIndex(it => it.idPizza === pizza.id).crust:"")}

                                                </React.Fragment>
                                            </div>
                                            <React.Fragment>
                                                {cart.find(item => item.id === pizza.id) ? (
                                                    <div>
                                                        <button className="btn btn-danger btn-sm"
                                                            onClick={() => this.changeQty("-", cart.find(item => item.id === pizza.id).id)}>
                                                            -
                                                    </button>
                                                        <button className="btn btn-secondary disabled btn-sm">
                                                            {cart.find(item => item.id === pizza.id).quantity}
                                                        </button>
                                                        <button className="btn btn-success btn-sm"
                                                            onClick={() => this.changeQty("+", cart.find(item => item.id === pizza.id).id)}>
                                                            +
                                                 </button>

                                                    </div>
                                                ) : (

                                                        <button className="btn btn-primary w-40 m-auto" onClick={() => this.addToCart(pizza.id, pizza)}>Add To Cart</button>
                                                    )}

                                            </React.Fragment>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                    <div className="container-fluid border float-right my-4 bg-light" style={{ width: "40%" }}>
                        <div className="row">
                            {cart.length === 0 ? <div className="h4 m-auto">Cart is Empty</div> : (
                                <React.Fragment>
                                    <div className="h4 m-auto">Cart</div> <br />
                                    <div className="container">
                                        {cart.map(item => (
                                            <div className="bg-light border">
                                                <div className="row">
                                                    <img src={item.image} className="col-6 py-5" alt="" />
                                                    <div className="col-6">
                                                        <span className="font-weight-bold">{item.name}</span>  <br />
                                                        {item.desc} <br />
                                                        {item.type === "Pizza" ? <span className="h6"> {item.size}|{item.crust}</span> : ""}  <br />
                                                        <button className="btn btn-danger btn-sm" onClick={() => this.changeQty("-", item.id)}>-</button>
                                                        <button className="btn btn-secondary btn-sm disabled" >{item.quantity}</button>
                                                        <button className="btn btn-success btn-sm" onClick={() => this.changeQty("+", item.id)}>+</button>
                                                    </div>
                                                </div>
                                            </div>
                                        ))}

                                    </div>

                                </React.Fragment>
                            )}
                        </div>
                    </div>
                </div>

            </React.Fragment>
        )
    }

    changeQty = (type, code) => {
        let s1 = { ...this.state };
        let qty = s1.cart.find(prod => prod.id === code).quantity;

        if (type === "+") {
            qty++;
        } else {

            qty--;
        }

        if (qty <= 0) {
            let index = s1.cart.findIndex(prod => prod.id === code);
            s1.cart.splice(index, 1);
        } else {
            s1.cart.find(prod => prod.id === code).quantity = qty;

        }
        this.setState(s1);
    }


    render() {
        let { items, view } = this.state;
        let vegPizzas = items.filter(it => it.veg === "Yes" && it.type === "Pizza");
        let nonVegPizzas = items.filter(it => it.veg === "No" && it.type === "Pizza");
        let sideDishes = items.filter(it => it.type === "SideDish");
        let otherItems = items.filter(it => it.type === "Burger Pizza" || it.type === "Dessert" || it.type === "Beverage");
        return (
            <div className="container-fluid" style={{ width: "90%" }}>
                {this.makeNavbar()}
                {view === 0 ? this.showItems(vegPizzas) : (view === 1 ? this.showItems(nonVegPizzas) : (
                    view === 2 ? this.showItems(sideDishes) : view === 3 ? this.showItems(otherItems) : ""
                ))}
            </div>
        )
    }


    makeDropDowns = (header, arr, class1, pizza, cart, name,value) => {
        return (
            <div className="form-group w-50 ">
                <select
                    className={"form-control " + class1}
                    disabled={cart.find(cartItem => cartItem.id === pizza.id)}
                    onChange={(event) => this.handleChange(event, pizza.id)}
                    name={name}
                    value={value}
                >
                    <React.Fragment>
                        <option value=""> {header} </option>
                        {arr.map(a => (<option value={a} >{a}</option>))}

                    </React.Fragment>
                </select>
            </div>
        )
    }

    show = (type) => {
        let s1 = { ...this.state };
        switch (type) {
            case "vegPizzas":
                s1.view = 0;
                break;
            case "nonVegPizzas":
                s1.view = 1;
                break;
            case "sideDishes":
                s1.view = 2;
                break;
            case "otherItems":
                s1.view = 3;
                break;
        }
        this.setState(s1);

    }


    makeNavbar = () => {

        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="#">MyFavPizza</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <a className="nav-link" href="#" onClick={() => this.show("vegPizzas")}>
                                Veg Pizza
                            </a>
                        </li>
                        <li className="nav-item active">
                            <a className="nav-link" href="#" onClick={() => this.show("nonVegPizzas")}>
                                Non-Veg Pizza
                            </a>
                        </li>
                        <li className="nav-item active">
                            <a className="nav-link" href="#" onClick={() => this.show("sideDishes")}>
                                Side Dishes
                            </a>
                        </li>
                        <li className="nav-item active">
                            <a className="nav-link" href="#" onClick={() => this.show("otherItems")}>
                                Other Items
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
        )
    }


}
export default MyComp;