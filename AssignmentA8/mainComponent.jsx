import React, { Component } from "react";
class MyComp extends Component {
  state = {
     products: [
          { "code": "PEP221", "name": "Pepsi", "price": 12, "instock": "Yes", "category": "Beverages" },
          { "code": "COK113", "name": "Coca Cola", "price": 18, "instock": "Yes", "category": "Beverages" },
          { "code": "MIR646", "name": "Mirinda", "price": 15, "instock": "No", "category": "Beverages" },
          { "code": "SLI874", "name": "Slice", "price": 22, "instock": "Yes", "category": "Beverages" },
          { "code": "MIN654", "name": "Minute Maid", "price": 25, "instock": "Yes", "category": "Beverages" },
          { "code": "APP652", "name": "Appy", "price": 10, "instock": "No", "category": "Beverages" },
          { "code": "FRO085", "name": "Frooti", "price": 30, "instock": "Yes", "category": "Beverages" },
          { "code": "REA546", "name": "Real", "price": 24, "instock": "No", "category": "Beverages" },
          { "code": "DM5461", "name": "Dairy Milk", "price": 40, "instock": "Yes", "category": "Chocolates" },
          { "code": "KK6546", "name": "Kitkat", "price": 15, "instock": "Yes", "category": "Chocolates" },
          { "code": "PER5436", "name": "Perk", "price": 8, "instock": "No", "category": "Chocolates" },
          { "code": "FST241", "name": "5 Star", "price": 25, "instock": "Yes", "category": "Chocolates" },
          { "code": "NUT553", "name": "Nutties", "price": 18, "instock": "Yes", "category": "Chocolates" },
          { "code": "GEM006", "name": "Gems", "price": 8, "instock": "No", "category": "Chocolates" },
          { "code": "GD2991", "name": "Good Day", "price": 25, "instock": "Yes", "category": "Biscuits" },
          { "code": "PAG542", "name": "Parle G", "price": 5, "instock": "Yes", "category": "Biscuits" },
          { "code": "MON119", "name": "Monaco", "price": 7, "instock": "No", "category": "Biscuits" },
          { "code": "BOU291", "name": "Bourbon", "price": 22, "instock": "Yes", "category": "Biscuits" },
          { "code": "MAR951", "name": "MarieGold", "price": 15, "instock": "Yes", "category": "Biscuits" },
          { "code": "ORE188", "name": "Oreo", "price": 30, "instock": "No", "category": "Biscuits" }
      ],
      view: 0,
      categories:["Beverages","Chocolates","Biscuits"],
      inStock:["Yes","No"],
      priceRange:["<10","10-20",">20"],
      sort: "",
      filter:{},
      billedProducts: [],
      /*
       0  normal view only navbar
        1  Product Lists view

       */
  };

  addToBill=(index)=> {
      let s1 = { ...this.state };
      let billedProdIndex = s1.billedProducts.findIndex(prod => prod.code === s1.products[index].code);
      if( billedProdIndex===-1)
      s1.billedProducts.push({...s1.products[index],quantity:0});
      else 
      s1.billedProducts[billedProdIndex].quantity ++;
      this.setState(s1);

  }

  closeBill=()=> {
      let s1 = { ...this.state };
      s1.billedProducts = [];
      this.setState(s1);

  }

    changeQty =(index,type)=> {
        let s1 = {...this.state};
        type==="+" ? (
            s1.billedProducts[index].quantity++
        ):(
            s1.billedProducts[index].quantity>1 ? (
                s1.billedProducts[index].quantity--

            ):(
             s1.billedProducts.splice(index, 1)

            )
        )
        this.setState(s1);

    }

    removeProduct=(index)=> {
        let s1 = {...this.state};
        s1.billedProducts.splice(index,1);
        this.setState(s1);
    }
  showProductsLists=()=> {
      let s1 = {...this.state};
      s1.view = 1;
      this.setState(s1);
  }

  sortBy=(type)=> {
    
      let s1 = {...this.state};
      s1.sort = type;
      switch(type) {
        case "price":
            s1.products.sort((pr1,pr2)=> pr1.price-pr2.price);
            break;
        case "code":
            s1.products.sort((pr1,pr2)=> pr1.code.localeCompare(pr2.code))
            break;
        case "name":
            s1.products.sort((pr1, pr2) => pr1.name.localeCompare(pr2.name))
            break;
        case "category":
            s1.products.sort((pr1, pr2) => pr1.category.localeCompare(pr2.category))
            break;
        case "instock":
            s1.products.sort((pr1, pr2) => pr1.instock.localeCompare(pr2.instock))
            break;
            
      }
    this.setState(s1);
  }

  handleChange =(e)=> {
      let s1 = {...this.state};
      let {currentTarget: input} = e;
        s1.filter[input.name] = input.value;
        this.setState(s1);
  }

  render() {
      let {products,categories, inStock, priceRange,sort,filter,billedProducts,view} = this.state;
      let prod1 = filter.category ?  products.filter(prod=> prod.category===filter.category): products;
      let prod2 = filter.instock ? prod1.filter(prod => prod.instock=== filter.instock): prod1;
      let prod3 = filter.priceRange ? prod2.filter(prod=> {
          if (filter.priceRange === "<10") return prod.price < 10;
          if (filter.priceRange === "10-20") return prod.price >10 && prod.price < 20;
          if (filter.priceRange ===">20") return prod.price>20;
        
      }): prod2;
         
         
    return (

      <div className="container">
          {this.makeNavbar()}
        {view===0 ? (""): (
        <React.Fragment>
        <div className="container bg-light my-3">
        <h3>Details of Current Bill</h3>
        items:{billedProducts.length},
        Quantity:{billedProducts.reduce((acc,curr)=> acc+ curr.quantity ,0)}, 
        Amount:{billedProducts.reduce((acc,curr)=>acc + curr.quantity*curr.price ,0)}

        {billedProducts.map((prod,index)=> (
            <div className="row border justify-content-between">
                <div className="col-6">
                    {prod.code} {prod.name} Price:{prod.price} Quantity:{prod.quantity} Value:{prod.price*prod.quantity}  
                </div>
                <div className="col-6">
                    <button className="btn btn-success btn-sm" onClick={()=>this.changeQty(index,"+")}>+</button>
                    <button className="btn btn-warning btn-sm" onClick={() => this.changeQty(index,"-")}>-</button>
                    <button className="btn btn-danger btn-sm" onClick={() => this.removeProduct(index)}>x</button>
                </div>
            </div>
        ))}
       {billedProducts.length>0? <button className="btn btn-primary" onClick={this.closeBill}>Close Bill</button>:""}
        </div>
        <div className="container">
            <h3 className="text-center mb-3">Product List</h3>
            <div className="d-flex justify-content-between">
                <span className="font-weight-bold d-inline"> Filter Products by:</span>
                {this.makeDropDowns("Select Category",categories,"category",filter.category)}
                {this.makeDropDowns("Select in Stock",inStock,"instock",filter.instock)}
                {this.makeDropDowns("Select Price Range",priceRange,"priceRange",filter.priceRange)} 
            </div>
        </div>
            <div className="row bg-dark text-light">
                <div className="col-2" onClick={() => this.sortBy("code")}>Code {sort==="code" ? "(X)" :""} </div>
                <div className="col-2" onClick={() => this.sortBy("name")}>Products {sort === "name" ? "(X)" : ""}</div>
                <div className="col-2" onClick={() => this.sortBy("category")}>Category {sort === "category" ? "(X)" : ""}</div>
                <div className="col-2" onClick={() => this.sortBy("price")}>Price {sort === "price" ? "(X)" : ""}</div>
                <div className="col-2" onClick={() => this.sortBy("instock")}>In Stock {sort === "instock" ? "(X)" : ""}</div>
                <div className="col-2"></div>
            </div>
        {prod3.map((prod,index) => (
            <div className="row bg-light border">
                <div className="col-2" >{prod.code}</div>
                <div className="col-2" >{prod.name}</div>
                <div className="col-2" >{prod.category}</div>
                <div className="col-2">{prod.price}</div>
                <div className="col-2" >{prod.instock}</div>
                <div className="col-2">
                    <button className="btn btn-secondary" onClick={()=>this.addToBill(index)} >
                        Add to Bill
                    </button>
                </div>
            </div>
        ))}
         </React.Fragment>)}
      </div>
    )
  }


    makeDropDowns = ( header, arr, name, selValue) => {
        return (
            <form className="form-group w-25 ">
                <select className="form-control" value={selValue} name={name} onChange={this.handleChange}>
                    <React.Fragment>
                        <option selected value="">{header}</option>
                        {arr.map(a => (<option value={a} >{a}</option>))}

                    </React.Fragment>
                </select>
            </form>
        )
    }

    makeNavbar = () => {
    
        return (
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">BillingSystem</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active" onClick={this.showProductsLists}>
                            <a class="nav-link" href="#">New Bill</a>
                        </li>
                       
                    </ul>
                </div>
            </nav>
        )
    }

   



  
}
export default MyComp;