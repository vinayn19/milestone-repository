import React, {Component} from "react";
import queryString from "query-string";
import LeftPanel from "./leftPanel";
import http from "./httpService";

class Books extends Component {
    state={ data:{} };

    async fetchData() {
        let queryParams =   queryString.parse(this.props.location.search);
        let {searchText} =  queryParams; 
        let apiURL = `/v1/volumes?q=${searchText}&startIndex=8&maxResults=8`;
        let response = await http.get(apiURL);
        let {data} = response;
        this.setState({data:data});
    }

     componentDidMount() {
        this.fetchData();
    }

     componentDidUpdate(prevProps,prevState) {
        if(prevProps!==this.props) {
            this.fetchData();
        }
    }

    handleOptionChange=(options)=> {
        this.callURL(options);

    }

    callURL=(options)=>{
        let queryParams =   queryString.parse(this.props.location.search);
        let {searchText} =  queryParams; 
        let baseURL = `/v1/volumes?q=${searchText}&startIndex=8&maxResults=8`;
        let searchStr = this.makeSearchString(options);
        this.props.history.push({
            pathname:baseURL,
            search: searchStr,
        })
    }

    makeSearchString=(options)=> {
        
    }


    render() {
        let queryParams = queryString.parse(this.props.location.search);
        let {items=[{volumeInfo:{imageLinks:{}}}]} = this.state.data;


        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3">
                    <LeftPanel options={queryParams} handleOptionChange={this.handleOptionChange}/>
                    </div>
                    <div className="col-9">
                        <div className="row">
                            {items.map(item=> (
                                <div className="col-3 border bg-success text-center">
                                    { item.volumeInfo.imageLinks ?  <img src={item.volumeInfo.imageLinks.thumbnail}/>: <span> </span>} <br/> 
                                    {item.volumeInfo.title} <br/>
                                   <strong>Authors:</strong>  {item.volumeInfo.authors ? item.volumeInfo.authors.join(","):"Not known"}

                                    {console.log(item.volumeInfo.imageLinks)} <br/>
                                    <button className="btn btn-secondary btn-sm">Add to My Books</button>
                                </div>
                            ))}
                        </div>
                             <div className="row m-2">
                                <div className="col-2">
                                    <button className="btn btn-primary">Previous</button>
                                </div>
                                <div className="col-8"></div>
                                <div className="col-2">
                                    <button className="btn btn-primary">Next</button>
                                </div>
                            </div>         
                    </div>
                </div>        
            </div>
        )
    }

   
}
export default Books;