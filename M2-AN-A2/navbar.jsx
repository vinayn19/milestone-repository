import React, { Component } from "react";
import {Link} from "react-router-dom";
import logo from "./logo.png";
class Navbar extends Component {
    state = {};


    render() {
        let finalStr = '&startIndex=8&maxResults=8';

        return (
            <nav className="navbar navbar-expand-sm navbar-light bg-light">
                <Link className="navbar-brand" to={`/`} >
                    <img src={logo} style={{width:"50px"}}/>
                </Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link className="nav-link" to={`/books?searchText=Harry Potter`+ finalStr} >Harry Potter </Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={`/books?searchText=Agatha Christie`+ finalStr} >Agatha Christie </Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={`/books?searchText=Premchand`+ finalStr}>Premchand</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={`/books?searchText=Jane Austen`+ finalStr}>Jane Austen</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={`/myshelf`}>My Books</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to={`/settings`}>Settings</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Navbar;