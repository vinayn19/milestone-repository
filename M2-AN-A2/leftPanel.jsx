import React, {Component} from "react";
class LeftPanel extends Component {
    state={
        langArr:['English','French','Hindi','Spanish','Chinese'],
        filtersArr:['Full Volume','Partial Volume','Free Google e-Books','Paid Google e-Books'],
        printTypesArr:['All','Books','Magazines'],
        orderByArr:['relevance','newest'],
        // book:{},
    };

    handleChange=()=>{
        let options = this.props.options;
        this.props.handleOptionChange(options);
    }


    render() {
        let {langArr,filtersArr,printTypesArr,orderByArr} = this.state;
        let {language,filterTypes,printTypes,orderBy} = this.props.options;
        return (
            <React.Fragment>
            {this.makeRadios('Language',langArr,'language',language)}
            {this.makeRadios('Filter',filtersArr,'filterTypes',filterTypes)}
            {this.makeRadios('Print Type',printTypesArr,'printTypes',printTypes)}
            {this.makeDropDowns('Order By',orderByArr,orderBy,'orderBy',"Order By")}
            </React.Fragment>
        )
    }

    makeDropDowns = (label,arr,value,name,header,error="") => (
        <React.Fragment>
            <label className="font-weight-bold">{label}</label> 
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map(opt => (<option >{opt}</option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }
    
                </div>

        </React.Fragment>
    )


    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid bg-light mt-3">
            <label className="m-0 font-weight-bold">{label}</label>
            {arr.map(opt=> (
                <div className="p-3 border">
            <div class="form-check">
                <input 
                class="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value===opt}
                onChange={this.handleChange}
                />
                <label class="form-check-label" for="flexRadioDefault1">
                    {opt}
                </label>
            </div>

                </div>
    
            ))}
    
        </div>
    )
}
export default LeftPanel;