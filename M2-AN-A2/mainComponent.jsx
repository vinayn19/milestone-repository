import React, {Component} from "react";
import Navbar from "./navbar";
import {Route,Redirect,Switch} from "react-router-dom";
import HomePage from "./homePage";
import Books from "./books";
class MainComponent extends Component {
    state={};

    render() {
        let {searchStr} = this.state;

        return (
            <React.Fragment>
                <Navbar/>
                <Switch>
                    <Route path={`/books`} render={(props)=> <Books {...props}  />}/>
                    <Route path={`/`} render={(props)=> <HomePage {...props} />}/>

                </Switch>

            </React.Fragment>

        )
    }
}
export default MainComponent;