import React, {Component} from "react";
import libcr from "./libcr.png";
import {Link} from "react-router-dom";
class HomePage extends Component {
    state={
        search:""
    };


    handleChange=(e)=> {
        let {currentTarget:input} = e;
        this.setState({search:input.value}) ;
    }
    
    handleSearchSubmit=()=> {
        let finalStr = '&startIndex=8&maxResults=8';
        this.props.history.push(`/books?searchText=${this.state.search}`+ finalStr);

    }

    render() {
        let {search } = this.state
        return (
            <div className="container w-75">
                <img src={libcr} className="d-block mx-auto" style={{width:"300px", height:"240px"}}/>
                <div className="row">
                    <div className="col-9">
                        {this.makeTextBox('','Search','search',search)}
                    </div>
                    <div className="col-3">
                        <button className="btn btn-primary mt-4" onClick={()=>this.handleSearchSubmit(search)}>
                            Search
                        </button>
                    </div>
                </div>
            </div>
        )
    }


    makeTextBox = (label, placeholder, name,value) => (
        <div className="form-group">
            <label>{label}</label>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
        </div>

    
    )


}
export default HomePage;