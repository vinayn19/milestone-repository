import React, {Component} from "react";
import http from "./services/httpService";
class DepositChequeForm extends Component {
    state={
        banksArr:[],
        formDetails:{
            name:this.props.user.name,
        },
    };

        async componentDidMount() {
            let response = await http.get(`/getBanks`);
            this.setState({banksArr:response.data,formErrors:{}}); 
        }

        handleChange=(e)=> {
            let s1 = {...this.state};
            let {currentTarget:input} = e;
            s1.formErrors[input.name] = this.makeFormErrors(input.name,input.value);
            s1.formDetails[input.name] = input.value;
            this.setState(s1);
        }

        makeFormErrors=(name,value)=> {
            switch(name) {
                case "chequeNumber":
                    return value && value.length >=10
                    ? ""
                    : "Enter your 10 digit Cheque Number";
                case "branch":
                    return value && value.length>=4
                    ? ""
                    : "Enter 4 digit code of branch"
            }

        }

        submitCheque= async()=> {
            let response = await http.post('/postCheque',this.state.formDetails);
            if(response.status===200) {
                alert('details Added Successfully');
                this.props.history.push('/customer');
            }
            
        }

    render() {
        let {banksArr,formDetails,formErrors={}} = this.state;
        let {chequeNumber,bankName,amount,branch}= formDetails;
        return (
            <div className="container">
                <h3 className="my-3">Deposit Cheque</h3>
                {this.makeTextBox("Cheque Number","Enter Cheque number","chequeNumber",chequeNumber,formErrors.chequeNumber)}
                {this.makeDropDowns("Bank Name",banksArr,bankName,"bankName","Select Bank")}
                {this.makeTextBox("Branch","Enter your branch","branch",branch,formErrors.branch)}
                {this.makeTextBox("Amount","Enter amount","amount",amount)}
                <button className="btn btn-primary ml-3" onClick={this.submitCheque}>Add Cheque</button>
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group mb-4 pl-3">
            <strong>{label}</strong> <span className="text-danger">*</span>
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
            <div className="d-flex justify-content-center">
            {error && <div className="text-danger alert alert-warning py-1" role="alert" >{error}</div> }

            </div>   
        </div>
    
    )

    makeDropDowns = (label,arr,value="",name,header,error="") => (
        <div className="container">
            <strong>{label}</strong><span className="text-danger">*</span> 
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value.trim()}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map((opt,index) => (<option key={index}> {opt} </option>))}
            
                    </select>
                    {error && <div className="text-danger alert alert-warning py-1" role="alert" >{error}</div> }

    
                </div>

        </div>
    )
}
export default DepositChequeForm;