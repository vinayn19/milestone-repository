import React, {Component} from "react";
import http from "./services/httpService";
import queryString from "query-string";

class AllNetBankingsAdmin extends Component {
    state={
        allNetBankings:{},
        banksArr:[],
        amountArr:["<10000",">=10000"],
    };
    
    async componentDidMount() {
        const [response1,response2] = await Promise.all([http.get(`/getAllNetBankings?page=1`),http.get(`/getBanks`)]);
        this.setState({allNetBankings:response1.data, banksArr:response2.data});
    }

    handleChange=(e)=>{
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        let queryParams = queryString.parse(this.props.location.search);
        queryParams.page = 1;
        queryParams[input.name] = input.value;
        this.callUrl(queryParams);

    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps !== this.props) {
        let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            let response = await http.get(`/getAllNetBankings${queryStr}`);
            let {data} = response;
            this.setState({allNetBankings:data});
        }
    }


    makeQueryString=(queryParams)=> {
        let {page,bank,amount} = queryParams;
        let queryString = "";
        queryString = this.addToQueryString(queryString,"page",page);
        queryString = this.addToQueryString(queryString,"bank",bank);
        queryString = this.addToQueryString(queryString,"amount",amount);
        return queryString;
    }

        addToQueryString=(queryString,queryParam,queryValue)=>
        queryValue ?
         queryString ? queryString + `&${queryParam}=${queryValue}`:`?${queryParam}=${queryValue}`
         : queryString;

    callUrl=(queryParams)=> {
        let baseUrl = `/allNet`;
        let queryString = this.makeQueryString(queryParams);
        this.props.history.push({
            pathname: baseUrl,
            search:queryString,
        })
     } 

    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {page} = queryParams;
        queryParams.page = +page + incr;
        // this.makeQueryString(queryParams);
        this.callUrl(queryParams);
    }

    render() {
        let {allNetBankings={},banksArr,amountArr} = this.state;
        let {items=[],page,totalNum} = allNetBankings;
        let itemsOnPage = 5;
        let totalPages = Math.ceil(+totalNum/itemsOnPage);
        let startPageIndex = (+page-1) * +itemsOnPage + 1;
        let finalPageIndex = startPageIndex + +itemsOnPage -1 <= totalNum ? startPageIndex + +itemsOnPage -1: totalNum ; 

        let queryParams = queryString.parse(this.props.location.search);
        let {bank,amount} = queryParams;
        return (
            <div className="container">
                <h4 className="my-3">All Net Banking Details</h4>
                <div className="row">
                <div className="col-3">
                    {this.makeRadios("Bank",banksArr,"bank",bank)}
                <div className="mt-2">
                        {this.makeRadios("Amount",amountArr,"amount",amount)}
                 </div>
                </div>
                <div className="col-9">
                    {startPageIndex}-{finalPageIndex} of {totalNum}

                        <div className="row border p-3 font-weight-bold mt-2">
                            <div className="col-3">Payee Name</div>
                            <div className="col-3">Amount</div>
                            <div className="col-3">Bank</div>
                            <div className="col-3">Comment</div>
                        </div>
                        {items.map((item,index)=> (
                        <div className="row border p-3" style={index%2===0 ? {backgroundColor:"#ededed"}:{backgroundColor:"white"}}>
                            <div className="col-3">{item.name}</div>
                            <div className="col-3">{item.amount}</div>
                            <div className="col-3">{item.bankName}</div>
                            <div className="col-3">{item.comment}</div>
                        </div>
                        ))}
                        <div className="row my-3">
                        <div className="col-1">
                        {page > 1 && 
                        <button className="btn btn-secondary" onClick={()=>this.changePage(-1)}>Previous</button>}
                        </div>
                        <div className="col-10"></div>
                        
                        <div className="col-1">
                        {page<totalPages &&
                        <button className="btn btn-secondary" onClick={()=>this.changePage(+1)}>Next</button>}
                        </div>
                </div>

                </div>

                </div>
            </div>
        )
    }

    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid border p-0">
            <div className="m-0 p-2 font-weight-bold ">{label}</div>
            {arr.map(opt=> (
                <div className="p-2 border">
                    <div class="form-check ">
                        <input 
                        class="form-check-input" 
                        type="radio" 
                        name={name} 
                        value={opt}
                        checked={value===opt}
                        onChange={this.handleChange}
                        />
                        <label class="form-check-label" for="flexRadioDefault1">
                            {opt}
                        </label>
                    </div>
                </div>
           ))}
        </div>
    )
 
}
export default AllNetBankingsAdmin;