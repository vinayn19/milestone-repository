import React, {Component} from "react";
import auth from "./services/authService";
import http from "./services/httpService";
class Login extends Component {
    state={
        loginInfo:{},
        errors:{}
    };

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        s1.errors[input.name] = this.makeLoginErrors(input.name,input.value);
         s1.loginInfo[input.name]= input.value;
        this.setState(s1);
    }

    makeLoginErrors=(name,value)=>{
        switch(name) {
            case "name":
                return value 
                ? ""
                :"username is required";
            case "password":
                return value
                ? value.length >= 7 
                ? "" 
                :"password length should be minimum 7 characters"
                :"password is required";
        }
    }

    async loginUser() {
        let response = await http.post('/login',this.state.loginInfo);
            let {data} = response;
            auth.login(data);
         window.location = data.role === "manager" ?'/admin': '/customer';

    }

    validateLoginForm=()=> {
        let {loginInfo, errors} = this.state;

        return Object.values(loginInfo).length===2 && Object.values(errors).findIndex(er=> er!=="")===-1;
    }
     

    render() {
        let {errors={},loginInfo} = this.state;
        let {name,password} = loginInfo;

        return (
            <div className="w-50 container text-center">
                <h3 className="my-3">Welcome to GBI Bank</h3>
                {this.makeTextBox("Username","Enter Username","name",name,errors.name)}
                {this.makeTextBox("Password","Enter Password","password",password,errors.password)}
                <button className="btn btn-primary my-2" disabled={!this.validateLoginForm()} onClick={()=>this.loginUser()}>Login</button>
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <label className="font-weight-bold">{label}</label>
            <input
                type={name==="password"? "password" : "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
            {error && <div className="text-danger alert alert-warning py-1" role="alert" >{error}</div> }
            {name==="name" && <small className="text-muted" >We will never share your user name with anyone else</small>}
        </div>
    
    )
}
export default Login;