import React, {Component} from "react";
import http from "./services/httpService";

class CustomerDetails extends Component {
    state={
        monthsArr : ['January','February','March','April','May','June','July','August','September','October','November','December'],
        yearsArr:[1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2000,2002,2003,2004,2005],
        daysArr:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
        genderArr:["Male","Female"],
        stateCitiesArr:[],
        details:{},
    };

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        if(input.name.includes("birth")) {
            s1.details.dob = s1.details.dob.split('-').map(ele=> ele=="")===-1 ? s1.details.dob : this.createDateString(s1.details.dob,input.name,input.value) ;
        }
        else {
            s1.details[input.name] = input.value;
        }

        this.setState(s1);
    }

    createDateString =(dob,name,value)=> {
        let dobArr = dob.split('-');
        switch(name) {
            case "birthDate":
            dobArr[0] = value;
            break;
            case "birthMonth":
            dobArr[1] = value;
            break;
            case "birthYear":
            dobArr[2] = value;
            break;

        }
        return dobArr.join('-');
    }


    async componentDidMount() {
     let customerName = this.props.user.name; 
     const [response1,response2] = await Promise.all([http.get(`/getCustomer/${customerName}`), http.get(`/statecity`)]);
        this.setState({
                details : Object.keys(response1.data).length>0 ? response1.data: {dob:'--',name:this.props.user.name} ,
               stateCitiesArr :  response2.data
        })}

        addCustomerDetails=async ()=> {
            let response = await http.post(`/customerDetails`,this.state.details);
            if(response.status===200) {
                alert('Customer details submitted successfully !')
                this.props.history.push(`/customer`);
            }
        }
    
    render() {
        let {genderArr,details={},monthsArr,yearsArr,daysArr,stateCitiesArr=[]} = this.state;
        let {gender,dob,PAN,addressLine1,addressLine2,state,city} = details;
        let statesArr = stateCitiesArr.map(stCity=>  stCity.stateName);
        let citiesArray = state ?  stateCitiesArr.find(stCity=> stCity.stateName=== state).cityArr: []; 
        return (
            <div className="container">
                <h4 className="my-3">Customer Details</h4>
                {this.makeRadios("Gender",genderArr,"gender",gender)}
                {this.makeDateDropDowns("Date of Birth",daysArr,monthsArr,yearsArr,dob,"birthDate","birthMonth","birthYear")}
                {this.makeTextBox("PAN","Enter PAN number","PAN",PAN)}
                   <div className="row">
                       <div className="col-6">
                            {this.makeTextBox("Address","Address 1","addressLine1",addressLine1)}
                       </div>
                       <div className="col-6">
                            {this.makeTextBox("‎‎","Address 2","addressLine2",addressLine2)}
                       </div>
                   </div>

                <div className="row">
                    <div className="col-6">
                        {this.makeDropDowns("State",statesArr,state,"state","Select State")}
                    </div>
                    <div className="col-6">
                        {this.makeDropDowns("City",citiesArray,city,"city","Select City")} 
                    </div>
                </div>
                <button className="btn btn-primary btn-sm ml-3" onClick={this.addCustomerDetails}>Add Details</button>
            </div>
        )
    }

    makeDropDowns = (label,arr,value="",name,header,error="") => (
        <div className="container mb-4">
            <strong>{label}</strong><span className="text-danger">*</span> 
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value.trim()}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map((opt,index) => (<option key={index}> {opt} </option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }
    
                </div>

        </div>
    )

    makeDateDropDowns = (label,arrDays,arrMonths,arrYears,dateString="",name1,name2,name3,error="") => (
        <div className="container mb-2">
                
                    <strong>{label}</strong> <span className="text-danger">*</span> 
                
            <div className="row">
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name1}
                            value={dateString.split("-")[0]}
                            onChange={this.handleChange}
                        >
                                {arrDays.map((opt,index) => (<option key={index}>{opt}</option>))}
                        </select>
                    </div>
                 </div>
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name2}
                            value={dateString.split("-")[1]}
                            onChange={this.handleChange}
                        >
                                {arrMonths.map((opt,index) => (<option  key={index}>{opt}</option>))}
                        </select>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name3}
                            value={dateString.split("-")[2]}
                            onChange={this.handleChange}
                        >
                                {arrYears.map((opt,index) => (<option  key={index}>{opt}</option>))}
                        </select>
                    </div>
                </div>
            </div>
                    <div className="d-flex justify-content-center" >
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}
            </div>  
        </div>
    )

   


    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid mb-3">
            <div className="row">
                <div className="col-3">
                    <label className="m-0 font-weight-bold">{label} <span className="text-danger">*</span> </label>
                </div>
            {arr.map((opt,index)=> (
               
                <div className="col-3"  key={index} >
            <div className="form-check">
                <input 
                className="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value===opt}
                onChange={this.handleChange}
                />
                <label className="form-check-label" >
                    {opt}
                </label>
            </div>
                </div>
    
            ))}

                <div className="col-4"> </div>
            </div>
    
        </div>
    )

   

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group mb-4 pl-3">
            <strong>{label}</strong>{label==="PAN" ? <span className="text-danger">*</span>:"" }
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default CustomerDetails;