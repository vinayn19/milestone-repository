import React, { Component } from "react";
import {Link} from "react-router-dom";
class Navbar extends Component {
    state = {};


    render() {
        let {user} = this.props;
        return (
            <nav className="navbar navbar-expand-sm navbar-light" style={{backgroundColor:"#ffce42"}}>
                <Link className="navbar-brand" to={user ? (user.role==="manager" ? `/admin` :`/customer`):`/login`} >Home</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                {!user && <ul className="navbar-nav">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/login`} >Login </Link>
                    </li>
                </ul>}
                { user && user.role==="customer" && 
                <React.Fragment>
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        View
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/viewCheque?page=1">Cheque</Link>
                        <Link className="dropdown-item" to="/viewNet?page=1">Net Banking</Link>
                    </div>
                </li>
                </ul>
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Details
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/customerDetails">Customer</Link>
                        <Link className="dropdown-item" to="/nomineeDetails">Nominee</Link>
                    </div>
                </li>
                </ul>
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Transaction
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/addPayee">Add Payee</Link>
                        <Link className="dropdown-item" to="/cheque">Cheque</Link>
                        <Link className="dropdown-item" to="/netBanking">NetBanking</Link>
                    </div>
                </li>
                </ul>
                </React.Fragment>
                }
                {user && user.role==="manager" &&
                <React.Fragment>
                    <ul className="navbar-nav">
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Customers
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <Link className="dropdown-item" to="/addCustomer">Add Customer</Link>
                            <Link className="dropdown-item" to="/allCustomers?page=1">View All Customers</Link>
                        </div>
                    </li>
                    </ul>
                    <ul className="navbar-nav">
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Transactions
                        </a>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <Link className="dropdown-item" to="/allCheque?page=1">Cheques</Link>
                            <Link className="dropdown-item" to="/allNet?page=1">NetBanking</Link>
                        </div>
                    </li>
                    </ul>
                </React.Fragment>
                }
    
                {user  &&
                <React.Fragment>
   
                <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                      Welcome {user.name} 
                    </li>
                </ul>
                <ul className="navbar-nav">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/logout`} >Logout </Link>
                    </li>
                </ul>

                </React.Fragment>
                
                }
            </nav>
        )
    }
}
export default Navbar;