import React, {Component} from "react";
import http from "./services/httpService";

class NetBankingTransaction extends Component {
    state={
        payeeDetails:[],
        transactionDetails: {name: this.props.user.name, bankName:"GBI"}
    };

    handleChange=(e)=>{
        let s1 =  {...this.state};
        let {currentTarget: input} = e;
        s1.transactionDetails[input.name] = input.value;
        this.setState(s1);
    }

    submitTransaction= async ()=>{
        let response  = await http.post('/postNet',this.state.transactionDetails);
        if(response.status===200) {
            alert("details added Sucessfully");
            this.props.history.push(`/customer`);
        }
    }

    async componentDidMount() {
        let {name} = this.props.user;
        let response = await http.get(`/getPayees/${name}`)
        let {data} = response;
        this.setState({payeeDetails:data});
    }

    render() {
        let {payeeDetails,transactionDetails} = this.state;
        let {payeeName,amount,comment} = transactionDetails;
        let payeeArr = payeeDetails.map(payee=> payee.payeeName);
        return (
            <div className="container">
                <h3 className="my-3">Net Banking Details</h3>
                {this.makeDropDowns("Payee Name",payeeArr,payeeName,"payeeName","Select Payee")}
                {this.makeTextBox("Amount","Enter Amount","amount",amount)}
                {this.makeTextBox("Comment","Enter Comment","comment",comment)}
                <button className="btn btn-primary ml-3" onClick={this.submitTransaction}>Add Transaction</button>
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group mb-4 pl-3">
            <strong>{label}</strong> {label==="Amount" && <span className="text-danger">*</span> }
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )

    makeDropDowns = (label,arr,value="",name,header,error="") => (
        <div className="container">
            <strong>{label}</strong><span className="text-danger">*</span> 
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value.trim()}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map((opt,index) => (<option key={index}> {opt} </option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }
    
                </div>

        </div>
    )

   
}
export default NetBankingTransaction;