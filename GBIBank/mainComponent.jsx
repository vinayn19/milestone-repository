import React, {Component} from "react";
import Navbar from "./navbar";
import Login from "./login";
import {Route,Switch,Redirect} from "react-router-dom";
import CustomerDetails from "./customerDetails";
import auth from "./services/authService";
import Logout from "./logout";
import AllChequesCustomer from './allChequesCustomer';
import AllNetBankingsCustomer from "./allNetBankingsCustomer";
import CustomerHomePage from './customerHomePage';
import NomineeDetails from "./nomineeDetails";
import DepositChequeForm from "./depositCheque";
import NetBankingTransaction from "./netBankingTransaction";
import AddPayee from "./addPayee";
import AllCustomers from "./allCustomers";
import AdminHomePage from "./adminHomePage";
import AddCustomer from "./addCustomer";
import AllChequesAdmin from "./AllChequesAdmin";
import AllNetBankingsAdmin from "./allNetBankingsAdmin";
import NotAllowed from "./notAllowed";

class MainComponent extends Component {

    render() {
        let user = auth.getUser();
        return (
            <div className="container-fluid px-0">
                <Navbar user={user}/>
                <div className="container">
                    <Switch>
                        <Route path='/login' render={props=> <Login />}/>
                        <Route path='/addCustomer' render={props=> user ? user.role==="manager" ?<AddCustomer user={user} {...props}/>:<Redirect to='/notAllowed'/>:<Redirect to='/login'/>}/>
                        <Route path='/allCheque' render={props=> user ? user.role==="manager" ? <AllChequesAdmin user={user} {...props}/>:<Redirect to='/notAllowed'/>:<Redirect to='/login'/>}/>
                        <Route path='/allNet' render={props=>user ? user.role==="manager" ? < AllNetBankingsAdmin user={user} {...props}/>:<Redirect to='/notAllowed'/>:<Redirect to='/login'/>}/>
                        <Route path='/logout' render={props=> <Logout user={user}/>}/>
                        <Route path='/allCustomers' render={props=> user ? user.role==="manager" ? <AllCustomers user={user} {...props}/> :<Redirect to='/notAllowed'/>:<Redirect to='/login'></Redirect>}/>
                        <Route path='/viewNet' render={props=>user ? user.role==="customer" ?  <AllNetBankingsCustomer user={user} {...props}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/viewCheque' render={props=>user ? user.role==="customer" ?  <AllChequesCustomer {...props} user={user}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/cheque' render={props=>user ? user.role==="customer" ? <DepositChequeForm user={user} {...props}/> : <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/nomineeDetails' render={props=>user ? user.role==="customer" ? <NomineeDetails user={user} {...props}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/> 
                        <Route path='/customerDetails' render={props=>user ? user.role==="customer" ? <CustomerDetails user={user} {...props}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/netBanking' render={props=>user ? user.role==="customer" ? <NetBankingTransaction user={user} {...props}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/addPayee' render={props=>user ? user.role==="customer" ? <AddPayee user={user} {...props}/> : <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/admin' render={props=> user ? user.role==="manager" ?<AdminHomePage user={user} {...props}/>: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/customer' render={props=> user ? user.role==="customer" ?<CustomerHomePage />: <Redirect to='/notAllowed'/>: <Redirect to='/login'/>}/>
                        <Route path='/notAllowed' render={props=> <NotAllowed />}/>
                        <Redirect path="/" to="/login"/>
                    </Switch>
                </div>
            </div>
        )
    }

   
}
export default MainComponent;