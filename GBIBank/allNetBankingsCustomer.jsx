import React, {Component} from "react";
import http from "./services/httpService";
import queryString from "query-string";

class AllNetBankingsCustomer extends Component {
    state={
        allNetBankings:{}
    };
    
    async componentDidMount() {
        let {name} = this.props.user;
        let response = await http.get(`/getNetBankingByName/${name}?page=1`);
        let {data} = response;
        this.setState({allNetBankings:data});
    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps !== this.props) {
        let {name} = this.props.user;
            let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            let response = await http.get(`/getNetBankingByName/${name}${queryStr}`);
            let {data} = response;
            this.setState({allNetBankings:data});
        }
    }


    makeQueryString=(queryParams)=> {
        let {page} = queryParams;
        let queryString = "";
        queryString = this.addToQueryString(queryString,"page",page);
        return queryString;
    }

        addToQueryString=(queryString,queryParam,queryValue)=>
         queryString ? `&${queryParam}=${queryValue}`:`?${queryParam}=${queryValue}`;

    callUrl=(queryParams)=> {
        let baseUrl = `/viewNet`;
        let queryString = this.makeQueryString(queryParams);
        this.props.history.push({
            pathname: baseUrl,
            search:queryString,
        })
     } 

    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {page} = queryParams;
        queryParams.page = +page + incr;
        // this.makeQueryString(queryParams);
        this.callUrl(queryParams);
    }

    render() {
        let {allNetBankings} = this.state;
        let {items=[],page,totalNum} = allNetBankings;
        let itemsOnPage = 5;
        let totalPages = Math.ceil(+totalNum/itemsOnPage);
        let startPageIndex = (+page-1) * +itemsOnPage + 1;
        let finalPageIndex = startPageIndex + +itemsOnPage -1 <= totalNum ? startPageIndex + +itemsOnPage -1: totalNum ; 
        return (
            <div className="container">
                <h4 className="my-3">All Net Banking Details</h4>
                {items.length>0 ? 
                <React.Fragment>
                {startPageIndex}-{finalPageIndex} of {totalNum}

                    <div className="row border p-3 font-weight-bold mt-2">
                        <div className="col-3">Payee Name</div>
                        <div className="col-3">Amount</div>
                        <div className="col-3">Bank</div>
                        <div className="col-3">Comment</div>
                    </div>
                    {items.map((item,index)=> (
                    <div className="row border p-3" style={index%2===0 ? {backgroundColor:"#ededed"}:{backgroundColor:"white"}}>
                        <div className="col-3">{item.name}</div>
                        <div className="col-3">{item.amount}</div>
                        <div className="col-3">{item.bankName}</div>
                        <div className="col-3">{item.comment}</div>
                    </div>
                    ))}
                    <div className="row my-3">
                    <div className="col-1">
                    {page > 1 && 
                    <button className="btn btn-secondary" onClick={()=>this.changePage(-1)}>Previous</button>}
                    </div>
                    <div className="col-10"></div>
                    
                    <div className="col-1">
                    {page<totalPages &&
                    <button className="btn btn-secondary" onClick={()=>this.changePage(+1)}>Next</button>}
                    </div>
                </div>

                </React.Fragment>
                : <span className="text-danger font-weight-bold">No Transactions to show</span>
                }
            </div>
        )
    }
}
export default AllNetBankingsCustomer;