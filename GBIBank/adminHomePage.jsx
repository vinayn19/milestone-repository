import React, {Component} from "react";
import bankpic from "./bankpic.jpg";
class AdminHomePage extends Component {
    state={};


    render() {
        return (
            <div className="container text-center">
               <div className=" h3 my-4 font-weight-bold" style={{color:"#ff7518"}}>Welcome to GBI BANK</div> 
                 <img src={bankpic}  style={{height:"340px"}} alt="bank-picture"/>
            </div>
        )
    }
}
export default AdminHomePage;