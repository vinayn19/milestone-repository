import React, {Component} from "react";
class NotAllowed extends Component {
    state={};


    render() {
        return (
            <div className="text-danger my-3 font-weight-bold h4">
                This Functionality is not available <br/>
            </div>
        )
    }
}
export default NotAllowed;