import React, {Component} from "react";
import http from "./services/httpService";
class AddPayee extends Component {
    state={
        payeeDetails:{name:this.props.user.name},
        bankOptions:["Same Bank", "Other Bank"],
        bankOptionsArr:["SBI","ICICI","HDFC","AXIS","DBS"],
    };

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget:input} = e;
        if(input.name==="bankName") {
            s1.payeeDetails[input.name] = input.value==="Same Bank" ? "GBI": input.value; 
        }else 
        s1.payeeDetails[input.name] = input.value;
        this.setState(s1);
    }

    addPayee= async(payeeName)=> {
        let response = await http.post('/addPayee',this.state.payeeDetails);
        if(response.status===200) {
            alert(`Payee added to your list :: ${payeeName}`)
            this.props.history.push('/customer');
        }
        
    }

    render() {
        let {payeeDetails,bankOptions,bankOptionsArr} = this.state;
        let {name,payeeName,IFSC,accNumber,bankName} = payeeDetails;
        return (
            <div className="container">
                <h3 className="my-3">Add Payee</h3>
                {this.makeTextBox("Payee Name","Enter payee name","payeeName",payeeName)}
                {this.makeTextBox("Account Number","Enter payee account number","accNumber",accNumber)}
                {this.makeRadios(bankOptions,"bankName",bankName)}
                {bankName!=="GBI" && bankName &&
                <React.Fragment>
                    {this.makeDropDowns("",bankOptionsArr,bankName,"bankName","Select Bank")}
                    {this.makeTextBox('IFSC Code','Enter IFSC Code',"IFSC",IFSC)}
                </React.Fragment>
                }
                <button className="btn btn-primary ml-3 my-2" onClick={()=>this.addPayee(payeeName)}>Add Payee</button>
                
            </div>
        )
    }

   

    makeDropDowns = (label,arr,value="",name,header,error="") => (
        <div className="container my-3">
            <strong>{label}</strong>
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value.trim()}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map((opt,index) => (<option key={index}> {opt} </option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }
    
                </div>

        </div>
    )

    makeRadios=(arr,name,value)=> (
        <div className="container-fluid">
            {arr.map(opt=> (
            <div class="form-check">
                <input 
                class="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value==="GBI" || !value ? opt==="Same Bank":opt==="Other Bank"}
                onChange={this.handleChange}
                />
                <label class="form-check-label" for="flexRadioDefault1">
                    {opt}
                </label>
            </div>
    
            ))}
    
        </div>
    )
    

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group mb-4 pl-3">
            <strong>{label}</strong> <span className="text-danger">*</span>
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default AddPayee;