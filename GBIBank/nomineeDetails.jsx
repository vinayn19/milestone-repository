import React, {Component} from "react";
import http from "./services/httpService";

class NomineeDetails extends Component {
    state={
        monthsArr : ['January','February','March','April','May','June','July','August','September','October','November','December'],
        yearsArr:[1980,1981,1982,1983,1984,1985,1986,1987,1988,1989,1990,1991,1992,1993,1994,1995,1996,1997,1998,1999,2000,2000,2002,2003,2004,2005],
        daysArr:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
        details:{},
        genderArr:["Male","Female"],

    };

    handleChange=(e)=> {
        let {currentTarget:input} = e;
        let s1 = {...this.state};
        if(input.name.includes("birth")) {
            s1.details.dob = s1.details.dob.split('-').map(ele=> ele=="")===-1 ? s1.details.dob : this.createDateString(s1.details.dob,input.name,input.value) ;
        }
        else if(input.name==="jointsignatory") {
            s1.details[input.name] = input.checked ? true: false; 
        }
        else {
            s1.details[input.name] = input.value;
        }

        this.setState(s1);
    }

 

    createDateString =(dob,name,value)=> {
        let dobArr = dob.split('-');
        switch(name) {
            case "birthDate":
            dobArr[0] = value;
            break;
            case "birthMonth":
            dobArr[1] = value;
            break;
            case "birthYear":
            dobArr[2] = value;
            break;

        }
        return dobArr.join('-');
    }

    async componentDidMount() {
        let {name} =  this.props.user;
        let response = await http.get(`/getNominee/${name}`);
        this.setState({
            details:Object.keys(response.data).length>0 ? response.data: {dob:'--',name:this.props.user.name}
        });
    }


    addNominee= async ()=> {
        let response = await http.post(`/nomineeDetails`,this.state.details);
        if(response.status===200) {
            alert("Nominee created successfully!");
            this.props.history.push('/customer');
        }
    }

    render() {
        let {details,genderArr,daysArr,monthsArr,yearsArr} = this.state;
        let {nomineeName,gender,dob,relationship,jointsignatory} = details;
        return (
            <div className="container text-left">
                <h4 className="my-3">Nominee Details</h4>
                {this.makeTextBox("Name","Enter the name of nominee","nomineeName",nomineeName)}
                {this.makeRadios("Gender",genderArr,"gender",gender)}
                {this.makeDateDropDowns("Date of Birth",daysArr,monthsArr,yearsArr,dob,"birthDate","birthMonth","birthYear")}
                {this.makeTextBox("Relationship","Nominee relationship","relationship",relationship)}
                {Object.keys(details).length===2 
                && this.makeCheckBox("Join Signatory","jointsignatory",jointsignatory)}
                <button className="btn btn-primary ml-4 my-3 btn-sm" onClick={this.addNominee}>Add Nominee</button>
            </div>
        )
    }

    makeCheckBox=(label,name,value)=> (
        <div className="form-check pl-5">
            <input 
                className="form-check-input" 
                type="checkbox" 
                name={name} 
                value={value && value===true ? false:true}
                checked={value===true ? true :false} 
                onChange={this.handleChange}
            />
            <label className="form-check-label">
                {label}
            </label>
        </div>

    )


    makeDateDropDowns = (label,arrDays,arrMonths,arrYears,dateString="",name1,name2,name3,error="") => (
        <div className="container mb-2">
                
                    <strong>{label}</strong> <span className="text-danger">*</span> 
                
            <div className="row">
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name3}
                            value={dateString.split("-")[2]}
                            onChange={this.handleChange}
                        >
                                {arrYears.map((opt,index) => (<option  key={index}>{opt}</option>))}
                        </select>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name2}
                            value={dateString.split("-")[1]}
                            onChange={this.handleChange}
                        >
                                {arrMonths.map((opt,index) => (<option  key={index}>{opt}</option>))}
                        </select>
                    </div>
                </div>
                <div className="col-4">
                    <div className="form-group">
                        <select
                            className="form-control"
                            name={name1}
                            value={dateString.split("-")[0]}
                            onChange={this.handleChange}
                        >
                                {arrDays.map((opt,index) => (<option key={index}>{opt}</option>))}
                        </select>
                    </div>
                 </div>
            </div>
                    <div className="d-flex justify-content-center" >
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}
            </div>  
        </div>
    )

   

    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid mb-3">
            <div className="row">
                <div className="col-3">
                    <label className="m-0 font-weight-bold">{label} <span className="text-danger">*</span> </label>
                </div>
            {arr.map((opt,index)=> (
               
                <div className="col-3"  key={index} >
            <div className="form-check">
                <input 
                className="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value===opt}
                onChange={this.handleChange}
                />
                <label className="form-check-label" >
                    {opt}
                </label>
            </div>
                </div>
    
            ))}

                <div className="col-4"> </div>
            </div>
    
        </div>
    )

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group mb-4 pl-3">
            <strong>{label}</strong>{label==="Relationship" ? <span className="text-danger">*</span>:"" }
                    <input
                        type="text"
                        className="form-control"
                        name={name}
                        value={value}
                        placeholder={placeholder}
                        onChange={this.handleChange}
                    />
            <div className="d-flex justify-content-center">
                      {error && (<div className="text-danger" role="alert" style={{marginRight:"120px"}}>{error}</div>  )}

            </div>   
        </div>
    
    )
}
export default NomineeDetails;