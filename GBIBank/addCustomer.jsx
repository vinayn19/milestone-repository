import React, {Component} from "react";
import http from "./services/httpService";
class AddCustomer extends Component {
    state={
        customerDetails:{},
        formErrors:{},
    };

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget:input} = e;
        if(input.name==="confirmPassword")
        s1[input.name] = input.value;
        else
        s1.customerDetails[input.name] = input.value;

        s1.formErrors[input.name] = this.makeFormErrors(input.name,input.value);
        this.setState(s1);
    }

    makeFormErrors=(name,value)=>{
        switch(name){
            case "name":
                return value 
                ? ""
                :"Please enter the name of customer"; 
            case "password":
                return value 
                ? value.length>=7 
                ? ""
                :"Minimum length is 7 characters"
                :"Password cannot be blank"
            
            case "confirmPassword":
                return value && value===this.state.customerDetails.password
                ? ""
                :"Password does not match"

        }
        

    }

    createCustomer= async()=> {
        let response = await http.post('/register',this.state.customerDetails);
        if(response.status===200){
            alert('Customer added successfully')
            this.props.history.push('/admin');
        }

    }


    render() {
        let {customerDetails,confirmPassword,formErrors} = this.state;
        let {name,password}= customerDetails;
        return (
            <div className="container">
                <h4 className="my-3">New Customer</h4>
                {this.makeTextBox("Name","Enter customer name","name",name,formErrors.name)}
                {this.makeTextBox("Password","Enter Password","password",password,formErrors.password)}
                {this.makeTextBox("Confirm Password","Confirm your password","confirmPassword",confirmPassword,formErrors.confirmPassword)}
                <button className="btn btn-primary" onClick={this.createCustomer}>Create</button>
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group my-3">
            <label>{label}</label>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
            {error && <div className="text-danger alert alert-warning py-1" role="alert" >{error}</div> }

        </div>
    
    )
}
export default AddCustomer;