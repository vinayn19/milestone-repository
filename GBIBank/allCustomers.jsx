import React, {Component, useState} from "react";
import http from "./services/httpService";
import queryString from "query-string";
class AllCustomers extends Component {
    state={
        allCustomers:[]
    };

    async componentDidMount() {
        let response = await http.get('/getCustomers?page=1');
        let {data} = response;
        this.setState({allCustomers:data});
    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps !== this.props) {
            let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            let response = await http.get(`/getCustomers${queryStr}`);
            let {data} = response;
            this.setState({allCustomers:data});
        }
    } 


    makeQueryString=(queryParams)=> {
        let {page} = queryParams;
        let queryString = "";
        queryString = this.addToQueryString(queryString,"page",page);
        return queryString;
    }

        addToQueryString=(queryString,queryParam,queryValue)=>
         queryString ? `&${queryParam}=${queryValue}`:`?${queryParam}=${queryValue}`;

    callUrl=(queryParams)=> {
        let baseUrl = `/allCustomers`;
        let queryString = this.makeQueryString(queryParams);
        this.props.history.push({
            pathname: baseUrl,
            search:queryString,
        })
     } 

    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {page} = queryParams;
        queryParams.page = +page + incr;
        // this.makeQueryString(queryParams);
        this.callUrl(queryParams);
    }


    render() {
        let {page,items:customers=[],totalNum} = this.state.allCustomers;
        let itemsOnPage = 5;
        let totalPages = Math.ceil(+totalNum/itemsOnPage);
        let startPageIndex = (+page-1) * +itemsOnPage + 1;
        let finalPageIndex = startPageIndex + +itemsOnPage -1 <= totalNum ? startPageIndex + +itemsOnPage -1: totalNum ; 
         return (
            <div  className="container">
                   <h4 className="my-3">All Customers Details</h4> 
                {startPageIndex}-{finalPageIndex} of {totalNum}
                        <div className="row border p-3 mt-2 font-weight-bold">
                        <div className="col-2">Name</div>
                        <div className="col-2">State</div>
                        <div className="col-2">City</div>
                        <div className="col-3">PAN</div>
                        <div className="col-3">DOB</div>
                    </div>
                {customers.map((cust,index)=> (
                    <div className="row border p-3 " style={index%2===0 ? {backgroundColor:"#ededed"}:{backgroundColor:"white"}}>
                          <div className="col-2">{cust.name}</div>
                        <div className="col-2">{cust.state}</div>
                        <div className="col-2">{cust.city}</div>
                        <div className="col-3">{cust.PAN}</div>
                        <div className="col-3">{cust.dob}</div>
                    </div>
                ))}
                <div className="row mt-3">
                    <div className="col-1">
                    {page > 1 && 
                    <button className="btn btn-secondary" onClick={()=>this.changePage(-1)}>Previous</button>}
                    </div>
                    <div className="col-10"></div>
                    
                    <div className="col-1">
                    {page<totalPages &&
                    <button className="btn btn-secondary" onClick={()=>this.changePage(+1)}>Next</button>}
                    </div>
                </div>
            </div>
        )
    }
}
export default AllCustomers;