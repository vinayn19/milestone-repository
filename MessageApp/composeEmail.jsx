import React, {Component} from "react";
class ComposeEmail extends Component {
    state={
        composedEmail: {},
        // ?this.props.totalMessages.find(msg=> msg.id === this.props.replyEmailId):{},
    };

    componentDidMount() {
        let composedEmail = {};
        let {replyEmailId,totalMessages} = this.props;
        if(replyEmailId) {
             composedEmail = {...totalMessages.find(msg=> msg.id === replyEmailId)};
             composedEmail.to = composedEmail.from;
             composedEmail.from = 'jack@test.com';
             composedEmail.subject = `Re:${composedEmail.subject}`;
             delete composedEmail.id;
        }else {
            composedEmail.from =  "jack@test.com";

        }
        this.setState({composedEmail:composedEmail});
    }


    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        s1.composedEmail[input.name] = input.value;
        
       this.setState(s1);
    }


    render() {
        let {to,subject,text} = this.state.composedEmail;
        let {replyEmailId} = this.props;

        return (
     
                <React.Fragment>
                    <div className="container">
                        {this.makeTextBox("To","Enter receiver's email id","to",to)}
                        {this.makeTextBox("Subject","Enter Subject","subject",subject)}
                        {this.makeTextBox("Message","Enter message","text",text)}
                        <button className="btn btn-primary btn-sm" onClick={()=>this.props.sendEmail(this.state.composedEmail)}>Send</button>
                    </div>
                </React.Fragment>
            
        )
    }

    makeTextBox = (label, placeholder, name,value) => (
        <div className="form-group">
            
            <label >{label}</label>
            {name==='text' ?
            <textarea 
            className='form-control'
            name={name}
            value={value} 
            placeholder={placeholder}
            onChange={this.handleChange}
            cols="70" 
            rows="5"
            >
            </textarea>   
        :    
        <input
            type="text"
            className="form-control"
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
            // onKeyPress={event => event.key === 'Enter' && this.sendEmail()}     
        />
        
        }
        </div>
    
    )
}
export default ComposeEmail;