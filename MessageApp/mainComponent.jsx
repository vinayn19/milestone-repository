import React, {Component} from "react";
import Navbar from "./navbar";
import ShowEmail from "./showEmail";
import ComposeEmail from "./composeEmail";
class MainComponent extends Component {
    state={
       totalMessages :  [
    {id: 121,sent: false,from: "tweets@twitter.com",to: "jack@test.com",subject: "18 tweets from those you follow",text: "Go to your twitter page and see the tweets from those you follow.",folder: "Social"},
    {id: 141,sent: true,from: "jack@test.com",to: "mary@test.com",subject: "Bug 461 in Customer Flow",text:"When the checkbox is left unchecked and the option Important is selected in the dropdown, clicking on Submit, shows no results.",folder: "Sent"},
    {id: 158,sent: false,from: "email@facebook.com",to: "jack@test.com",subject: "New post from William Jones",text:"William Jones has just uploaded a new post -How i loved the Avengers Endgame.",folder: "Social"},
    {id: 177,sent: true,from: "jack@test.com",to: "williams@test.com",subject: "Movie tomorrow",text: "Avengers Endgame is releasing tomorrow. Wanna see.",folder: "Sent"},
    {id: 179,sent: false,from: "williams@test.com",to: "jack@test.com",subject: "Re: Movie tomorrow",text:"The movie is supposed to be a blast. Lets do the 8:30 show. Want to have a quick bite before.",folder: "Inbox"},
    {id: 194,sent: false,from: "retweet@twitter.com",to:"jack@test.com",subject: "Your tweet has been retweeted by Thomas",text:"Your tweet on the Marvel Superheroes and Avengers has been retweeted bt Thomas. It has now 41 retweets and 27 likes.",folder: "Social"},
    {id: 204,sent: true,from: "jack@test.com",to: "jack@test.com",subject: "To do on Friday",text: "Test the bugs on the employee form in Release 0.7.9 and fix them.",folder: "Work"},
    {id: 255,sent: true,from: "mary@test.com",to: "jack@test.com",subject: "Release 0.8.4 deployed",text: "Release 0.8.4 has been deployed in the test environment.",folder: "Inbox"},
    {id: 278,sent: false,from: "mary@test.com",to: "jack@test.com",subject: "Re: Bug 461 in Customer Flow",text:"The bug has been fixed in the release 0.8.7. \nPlease test the issue and close it.\nCan you do it but tomorrow\nMary",folder: "Work"},
    {id: 281,sent: true,from: "jack@test.com",to: "mary@test.com",subject: "Re: Re: Bug 461 in Customer Flow",text: "Bug 461 has been closed.\nRegards,\nJack",folder: "Sent"},
    {id: 289,sent: false,from: "email@facebook.com",to: "jack@test.com",subject: "5 Shares, 2 Posts from your friends",text:"Jack, while you were away, your friends are having fun on Facebook.\nDon't miss their posts.\nKeep up with your friends.",folder: "Social"}
],
    view:"Inbox",
    selectedEmails:[],
    moveToArr:['Inbox','Sent','Drafts','Work','Social'],
    moveTo:null,
    showEmailById: null,
    replyEmailId: null,
    searchedEmails:[],
    };

    handleChange=(e)=>{
        let s1 = {...this.state};
        let {currentTarget: input} = e;
            s1.selectedEmails = this.makeSelectedEmails(s1.selectedEmails,input.value);
        this.setState(s1); 
    }

    handleMoveTo=(e)=> {
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        s1[input.name] = input.value;
        let selMessagesIds = s1.selectedEmails.map(email=> email.id);
        s1.totalMessages = s1.totalMessages.map(msg=> {
            let index = selMessagesIds.findIndex(id=> id===msg.id);
            if(index>=0) {
                msg.folder = input.value;
                return msg;
            }else 
            return msg;
        })
        if(s1.searchedEmails.length >0)
        s1.searchedEmails = s1.searchedEmails.filter(email=> {
            let index = selMessagesIds.findIndex(id=> id=== email.id);
            return index===-1;
        })
        s1.selectedEmails = [];
        this.setState(s1);
    };

    makeSelectedEmails=(array,msg)=> {
        let emailReq = JSON.parse(msg);
        let index = array.findIndex(mg=> mg.id===emailReq.id);
        index>=0 ? array.splice(index,1):array.push(emailReq);
        return array;
    }
    
    changeView=(view)=> this.setState({view:view});
    
    composeEmail=()=> this.setState({view:"composeEmail",replyEmailId:null});
    

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <label>{label}</label>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
            {error ? (<div className="alert alert-danger" role="alert">{error}</div>  ):""}
        </div>
    
    )

    sendEmail=(composedEmail)=> {
        let s1 = {...this.state};

        composedEmail.sent = true;
        composedEmail.folder = 'Sent';
        let maxId = s1.totalMessages.reduce((acc,curr)=>  (curr.id >= acc.id ? curr : acc)).id;
        composedEmail.id = maxId+1; 
        s1.replyEmailId = null;
        console.log('fas;ljfd;asdfasdf');
        console.log(composedEmail);
        s1.totalMessages.push(composedEmail);
        s1.view = "Inbox";
        this.setState(s1);
    }

    handleSearch=(search)=>{
            let s1 = {...this.state};
            s1.view = "search";
            s1.searchedEmails = s1.totalMessages.filter(email=> email.to.includes(search) 
            || email.from.includes(search) || email.subject.includes(search)
            || email.text.includes(search));
            this.setState(s1);        
    }

    deleteSelEmail=()=>{
        let s1 = {...this.state};
        let messageIds = s1.selectedEmails.map(email=> email.id);
        s1.totalMessages =  s1.totalMessages.filter(msg=> messageIds.findIndex(id=> id===msg.id)===-1 );
        if(s1.searchedEmails.length >0)
         s1.searchedEmails = s1.searchedEmails.filter(msg=> messageIds.findIndex(id=> id===msg.id)===-1 );
        this.setState(s1);
    }

    openEmail=(id)=> {
        this.setState({view:'open-email',showEmailById:id})
    }

    replyEmail=(id)=> {
        let s1 = {...this.state};
        s1.view = 'composeEmail';
        s1.replyEmailId= id;
        this.setState(s1);

    }

    render() {
        let {view,totalMessages,searchedEmails,selectedEmails,moveToArr,moveTo,showEmailById,replyEmailId} = this.state;
        let  color1 = view==="Inbox" ? '#fdde6c':"";
        let color2 = view==="Sent" ? '#fdde6c':"";
        let color3 = view==="Drafts" ? '#fdde6c':"";
        let color4 = view==="Work" ? '#fdde6c':"";
        let color5 = view==="Social" ? '#fdde6c':"";
        let messages = view==="search" 
        ? searchedEmails 
        :  totalMessages.filter(msg=> msg.folder===view);

        let totalInboxMsgs = totalMessages.reduce((acc,curr)=> curr.folder==='Inbox' ? acc+1: acc,0);
        let totalSentMsgs = totalMessages.reduce((acc,curr)=> curr.folder==='Sent' ? acc+1: acc,0);
        let totalDraftsMsgs = totalMessages.reduce((acc,curr)=> curr.folder==='Drafts' ? acc+1: acc,0);
        let totalWorkMsgs = totalMessages.reduce((acc,curr)=> curr.folder==='Work' ? acc+1: acc,0);
        let totalSocialMsgs = totalMessages.reduce((acc,curr)=> curr.folder==='Social' ? acc+1: acc,0);
        moveTo='';
        return (
            <div className="container-fluid">
                 <Navbar handleSearch={this.handleSearch}/>

                <div className="row my-3" style={{width:'1300px',marginLeft:'15px'}}>
                    <div className="col-2">
                        <button className="btn btn-primary btn-sm mb-2" onClick={this.composeEmail}>Compose</button> <br/>
                       <div className='py-1 ' onClick={()=>this.changeView("Inbox")} style={{backgroundColor:`${color1}`,borderRadius:'0px  15px 15px 0px'}} role='button'>Inbox ({totalInboxMsgs})</div>
                       <div className='py-1' onClick={()=>this.changeView("Sent")} style={{backgroundColor:`${color2}`,borderRadius:'0px  15px 15px 0px'}} role='button'>Sent({totalSentMsgs}) </div>  
                       <div className='py-1' onClick={()=>this.changeView("Drafts")} style={{backgroundColor:`${color3}`,borderRadius:'0px  15px 15px 0px'}} role='button'>Drafts({totalDraftsMsgs})</div>  
                       <div className='py-1' onClick={()=>this.changeView("Work")} style={{backgroundColor:`${color4}`,borderRadius:'0px  15px 15px 0px'}} role='button'>Work({totalWorkMsgs})</div>  
                       <div className='py-1' onClick={()=>this.changeView("Social")} style={{backgroundColor:`${color5}`,borderRadius:'0px  15px 15px 0px'}} role='button'>Social({totalSocialMsgs})</div>  
                    </div>
                    <div className="col-10 bg-light">
                        
                        {view === "composeEmail" ? 
                         <ComposeEmail sendEmail={this.sendEmail} totalMessages={totalMessages} replyEmailId={replyEmailId}/>
                        : view==='open-email' 
                        ? <ShowEmail showEmailById={showEmailById} totalMessages={totalMessages} replyEmail={this.replyEmail}/>
                        :
                         <div className="container">
                            {selectedEmails.length>0 && 
                            <div className="row">
                                <div className="col-1">
                                <button className="btn btn-primary btn-sm" onClick={this.deleteSelEmail}>Delete</button> 
                                </div>
                                <div className="col-11">
                                {this.makeDropDowns("",moveToArr,moveTo,"moveTo","Move To")}
                                </div>
                            </div>
                            }
                            {messages.map((msg,index) =>
                                <div className="row border p-2 border-left-0 border-right-0" key={msg.id}
                                onClick={()=>this.openEmail(msg.id)} role={'button'} 
                                onMouseEnter={this.popEmailEffect} >
                                    <div className="col-1" >{this.makeCheckBox("selectedEmails",selectedEmails,msg)}</div>
                                    <div className="col-3">{msg.from==="jack@test.com" ? "me" : msg.from}</div>
                                    <div className="col-4 text-truncate">{msg.subject }</div>
                                    <div className="col-4 text-truncate">{msg.text }</div>
                                </div>
                            )}

                        </div>
                         }
                    </div>
                </div>


            </div>
        )
    }


makeDropDowns = (label,arr,value,name,header,error="") => (
    <div className="row w-50">
        <div className="col-2">{label}</div>
        <div className="col-10">
            <div className="form-group">
                <select
                    className="form-control custom-select-sm"
                    name={name}
                    value={value}
                    onChange={this.handleMoveTo}
                    
                >
                        <option value=""> {header} </option>
                        {arr.map(opt => (<option >{opt}</option>))}
        
                </select>
            {error && <div className="text-danger text-left"><small>{error}</small> </div> }

            </div>
        </div>
    </div>
)

    

    makeCheckBox=(name,valueArr,value)=> (
        <div className="form-check">

        <input 

             style={{width:'14px',height:'14px'}}
            className="form-check-input" 
            type="checkbox" 
            name={name} 
            value={JSON.stringify(value)}
            checked={valueArr.find(val=> val.id===value.id)}  
            onChange={this.handleChange}
        />
        
        </div>
    )

    makeTextBox = (label, placeholder, name,value) => (
        <div className="form-group">
            <label >{label}</label>
        <input
            type="text"
            className="form-control"
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={this.handleChange}
            // onKeyPress={event => event.key === 'Enter' && name=== 'search' && this.handleSearch()}     
        />
        </div>
    
    )
}
export default MainComponent;