import React, {Component} from "react";
class ShowEmail extends Component {
    state={};


    render() {

        let {showEmailById, totalMessages}  = this.props;
        let message = totalMessages.find(msg=> msg.id===showEmailById);
        return (
            <div className="container-fluid ">
            <div className="row w-50">
                <div className="col-6 p-1">From</div>
                <div className="col-6 p-1">{message.from}</div>
                <div className="col-6 p-1">To</div>
                <div className="col-6 p-1">{message.to}</div>
                <div className="col-6 p-1">Subject</div>
                <div className="col-6 p-1">{message.subject}</div>
                <div className="col-6 p-1">Message</div>
                <div className="col-6 p-1">{message.text.split('\n').map((msg)=> <span key={msg.id} >{msg} <br/></span>   )}</div>
            </div>        
            <button className="btn btn-secondary mt-2 " onClick={()=>this.props.replyEmail(message.id)}>Reply</button>
            </div>
        )
    }
}
export default ShowEmail;