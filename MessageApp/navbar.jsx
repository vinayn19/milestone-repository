import React, { Component } from "react";
import {Link} from "react-router-dom";
class Navbar extends Component {
    state = {
        search:"",
    };


  handleChange=(e)=> {
      let s1 = {...this.state};
      let {currentTarget:input} = e;
    s1[input.name] = input.value 
    this.setState(s1);
  }


    render() {
        let {search} = this.state;
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <Link className="navbar-brand" to={`/`} >MyMessages</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="form-inline">
                {this.makeTextBox("","Search","search",search)}
                </div>
            </nav>
        )
    }

   

    makeTextBox = (label, placeholder, name,value) => (
        <div className="form-group" >
            <label>{label}</label>
            <input
                type="text"
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
                onKeyUp={event => event.key === 'Enter' &&  this.props.handleSearch(this.state.search)}
                style={{width:"447px"}}
            />
        </div>
    
    )
}
export default Navbar;