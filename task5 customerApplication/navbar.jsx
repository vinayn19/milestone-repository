import React, { Component } from "react";
import {Link} from "react-router-dom";
class Navbar extends Component {
    state = {};


    render() {
        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <Link className="navbar-brand" to={`/`} >MyCompany</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link className="nav-link" to={`/allCustomers`} >All Customers </Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={`/add/customer`} >Add New Customer </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Navbar;