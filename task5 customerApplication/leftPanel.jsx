import React, {Component} from "react";
import queryString from "query-string";
import http from './httpService';

class LeftPanel extends Component {
    state={
        genderArr:['Male','Female'],
        cityArr:['Delhi','Noida','Gurgaon','Jaipur'],
        paymentOptArr:['Debit Card','Wallet','Credit Card'],
        sortByArr:['city','age','payment'],
    };

   handleChange=(e)=> {
    let queryParams = queryString.parse(this.props.location.search);
    let {currentTarget:input} = e;
    queryParams[input.name] = input.value;
    this.callURL(queryParams);

   }

  /*  async componentDidUpdate(prevProps,prevState) {
    if(prevProps !== this.props) {
        let queryString = this.props.location.search;
        let response = await http.get(`/customers${queryString}`);
        let {data} = response;
         this.props
    }
   } */

   callURL=(queryParams)=> {
       let queryStr = this.makeQueryString(queryParams);
       this.props.history.push({
           pathname:'/allCustomers',
           search: queryStr
       })

   }

   makeQueryString=(queryParams)=> {
    let {gender,city,payment,sortBy} = queryParams;
       let queryString = "";
        queryString = this.addToQueryString(queryString,'gender',gender);
        queryString = this.addToQueryString(queryString,'city',city);
        queryString = this.addToQueryString(queryString,'payment',payment);
        queryString = this.addToQueryString(queryString,'sortBy',sortBy);

        return queryString;
   }

   addToQueryString=(queryString,paramName,paramValue)=> 
    paramValue ?
        queryString?
        queryString = queryString + `&${paramName}=${paramValue}`:`?${paramName}=${paramValue}`
    : queryString
   
    resetAll=()=> {
        let queryParams={};
        this.callURL(queryParams);
    }

    render() {
        let {genderArr,paymentOptArr,cityArr,sortByArr} = this.state;
        let queryParams = queryString.parse(this.props.location.search);
        let {gender,city,payment,sortBy} = queryParams;
        return (
            <div className='container'>
                <button className="btn btn-primary btn-sm ml-3" onClick={this.resetAll}>Reset all</button>
                {this.makeRadios('Gender',genderArr,'gender',gender)}
                {this.makeRadios('Sort By',sortByArr,'sortBy',sortBy)}
                {this.makeRadios('City',cityArr,'city',city)}
                {this.makeRadios('Payment Options',paymentOptArr,'payment',payment)}
            </div>
        )
    }


    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid ">
            <div className="m-0 font-weight-bold p-2">{label}</div>
            {arr.map(opt=> (
                <div className='border'>
                    <div class="form-check p-2 ml-3 ">
                        <input 
                        class="form-check-input" 
                        type="radio" 
                        name={name} 
                        value={opt}
                        checked={value===opt}
                        onChange={this.handleChange}
                        />
                        <label class="form-check-label" for="flexRadioDefault1">
                            {opt}
                        </label>
                    </div>

                </div>
    
            ))}
    
        </div>
    )
}
export default LeftPanel;