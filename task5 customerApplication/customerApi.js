let express = require("express");
let app = express();
app.use(express.json());

//express.json() returns a piece of middleware and app.use, use the middleware
//request processing pipeline. 
//enable parsing of JSON object(for post) into the req(request) because by default its
// not enabled in Express.

let customers = require('./customerData').customers;
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});


let port = process.env.PORT || 2410;
app.listen(port, ()=> console.log(`Listening on the PORT.. ${port}`))

app.get('/customers',(req,res)=> {
  let {city,gender,payment,sortBy} = req.query;
  let customerArr = customers;
  if(city) customerArr = customerArr.filter(cust=> cust.city===city);
  if(gender) customerArr = customerArr.filter(cust=> cust.gender===gender);
  if(payment) customerArr = customerArr.filter(cust=> cust.payment===payment);
  if(sortBy) {
    if(sortBy==='city') customerArr = customerArr.sort((cust1,cust2)=> cust1.city.localeCompare(cust2.city)); 
    if(sortBy==='payment') customerArr = customerArr.sort((cust1,cust2)=> cust1.payment.localeCompare(cust2.payment));
    if(sortBy==='age') customerArr = customerArr.sort((cust1,cust2)=> +cust1.age - +cust2.age);
  }
 res.send(customerArr);  
})

app.get(`/customers/:id`,(req,res)=> {
  console.log(req.params);
  let id = req.params.id;
  const customer = customers.find(customer=> customer.id===id); 
  console.log(customer);
  res.send(customer);  
 })
 

app.post('/customers',(req,res)=> {
  let {id,name,city,age,gender,payment} = req.body;
  let customer = customers.find(customer=> customer.id===id);
  if(customer) res.status(404).send('The customer with given ID already exist');
   customer = {
    id: id,
    name: name,
    city: city,
    age: age,
    gender: gender,
    payment: payment,
  }
  customers.push(customer);
  res.send(customer);
})

app.put('/customers/:id',(req,res)=> {
  let customer = customers.find(customer => customer.id===req.params.id);  
  if(!customer) res.status(404).send('The customer with given ID does not exist');
  let index = customers.indexOf(customer);
  let {id,name,city,age,gender,payment} = req.body;

  customer.id = id;
  customer.name= name,
  customer.city= city,
  customer.age= age,
  customer.gender= gender,
  customer.payment= payment,

  
  customers[index] = customer;
  res.send(customers);
})



app.delete('/customers/:id',(req,res)=> {
  let customer = customers.find(customer => customer.id===req.params.id);  
  if(!customer) res.status(404).send('The customer with given ID does not exist');
  let index = customers.indexOf(customer);
  customers.splice(index,1);
  res.send(customers);
})

