import React, {Component} from "react";
import http from "./httpService";
import Navbar from "./navbar";
import {Switch,Route,Redirect} from "react-router-dom";
import AllCustomers from './allCustomers';
import AddCustomer from "./addCustomer";

class MainComponent extends Component {

    render() {
        return (
            <React.Fragment>
                <Navbar/>

                <Switch>
                    <Route path='/allCustomers' render ={(props)=> <AllCustomers {...props} />}/>
                    <Route path='/edit/customer/:id' render ={(props)=> <AddCustomer {...props} />}/>
                    <Route path='/add/customer' render ={(props)=> <AddCustomer {...props} />}/>
                    <Redirect from='/'  to='/allCustomers'/>

                </Switch>
            </React.Fragment>
        )
    }
}
export default MainComponent;