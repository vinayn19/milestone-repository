import React, { Component } from "react";
import http from './httpService';
import LeftPanel from "./leftPanel";
// import queryString from "query-string";

class AllCustomers extends Component {
    state = {};

    async componentDidMount() {
        this.fetchData();
    }

    fetchData = async () => {
        let queryStr = this.props.location.search;
        let response = await http.get(`/customers${queryStr}`);
        let { data } = response;
        this.setState({ customers: data });
    }

    async componentDidUpdate(prevProps, prevState) {
        if (prevProps !== this.props)
            this.fetchData();

    }

    editCustomer = (id) => {
        this.props.history.push(`edit/customer/${id}`);
    }

    deleteCustomer = async (id) => {
        let response = await http.deleteReq(`/customers/${id}`);
        console.log(response.data);
        let queryStr = this.props.location.search;
        console.log(queryStr);
        this.props.history.push(`/allCustomers${queryStr}`);
    }


    render() {
        let { customers = [] } = this.state;
        return (

            <div className='container my-3'>
                <div className="row">
                    <div className="col-3">
                        <LeftPanel {...this.props}/>
                    </div>
                    <div className="col-9">
                <h4>All Customers</h4>  
                <div className="row border bg-dark text-light">
                    <div className="col-2">Id</div>
                    <div className="col-2">Name</div>
                    <div className="col-2">City</div>
                    <div className="col-1">Age</div>
                    <div className="col-1">Gender</div>
                    <div className="col-2">Payment</div>
                    <div className="col-1"></div>
                    <div className="col-1"></div>
                </div>
                {customers.map(cust =>
                    <div className="row border">
                        <div className="col-2">{cust.id}</div>
                        <div className="col-2">{cust.name}</div>
                        <div className="col-2">{cust.city}</div>
                        <div className="col-1">{cust.age}</div>
                        <div className="col-1">{cust.gender}</div>
                        <div className="col-2">{cust.payment}</div>
                        <div className="col-1">
                            <button className="btn btn-primary btn-sm" onClick={() => this.editCustomer(cust.id)}>Edit</button>
                        </div>
                        <div className="col-1">
                            <button className="btn btn-primary btn-sm" onClick={() => this.deleteCustomer(cust.id)}> Delete</button>
                        </div>
                    </div>
                )}
                    </div>
                </div>


            </div>
        )
    }


}

export default AllCustomers;