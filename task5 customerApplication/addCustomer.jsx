import React, {Component} from "react";
import http from "./httpService";
class AddCustomer extends Component {
    state={ 
        genderArr:['Male','Female'],
        paymentOptArr:['Debit Card','Wallet','Credit Card'],
        cityArr:['Delhi','Noida','Gurgaon','Jaipur'],
        customer:{},
        
};

fetchData= async ()=> {
    let id = this.props.match.params.id;
    console.log(id);
    if(id) {
        console.log('in if');
        let response = await http.get(`/customers/${id}`);
        let {data} = response;
        this.setState({customer: data});
        
    } else {
        this.setState({customer:{}});
    }
}

     componentDidMount(){
        this.fetchData();

    }

     componentDidUpdate(prevProps,prevState) {
        if(prevProps!==this.props) {
            this.fetchData();

        }

    }

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget: input} = e;
       s1.customer[input.name] = input.value;
        this.setState(s1);
    }

    addCustomerDetails=async()=> {
        let id = this.props.match.params.id;
        let response = id  ? await http.put(`/customers/${id}`,this.state.customer): await http.post(`/customers`,this.state.customer);
        this.props.history.push('/allCustomers');
    }
    
    render() {
        let {customer={},genderArr,paymentOptArr,cityArr} = this.state;
        let {id,name,city,age,gender,payment} = customer;
        console.log(id,name,city,age,gender,payment);
        let {id :editId} = this.props.match.params;
        let heading = editId ? 'Edit Customer Details': 'Add New Customer';
        console.log('customer',customer);
        return (
            <div className='container'>
                <h4>{heading}</h4>
                {this.makeTextBox('Id','Enter Id','id',id)}
                {this.makeTextBox('Name','Enter customer name','name',name)}
                {this.makeTextBox('Age','Enter Age','age',age)}
                {this.makeDropDowns('City',cityArr,'city',city,'Select City')}
                {this.makeRadios('Gender',genderArr,"gender",gender)}
                {this.makeRadios('Payment',paymentOptArr,"payment",payment)}
                <button className="btn btn-primary" onClick={this.addCustomerDetails}> {editId ? <span> Update Details</span>: <span>Add Customer</span> }</button>
            </div>
        )
    }
    
    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid">
            <label className="m-0">{label}</label>
            {arr.map(opt=> (
            <div class="form-check">
                <input 
                class="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value===opt}
                onChange={this.handleChange}
                />
                <label class="form-check-label" for="flexRadioDefault1">
                    {opt}
                </label>
            </div>
    
            ))}
    
        </div>
    )
    
    makeDropDowns = (label,arr,name,value="",header,error="") => (
        <div>
            {label}
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map(opt => (<option >{opt}</option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }
    
                </div>

        </div>
    )


     makeTextBox = (label, placeholder, name,value="",error) => (
        <div className="form-group">
            <label>{label}</label>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                readOnly={name==='id' && this.props.match.params.id}
                onChange={this.handleChange}
            />
            {error ? (<div className="alert alert-danger" role="alert">{error}</div>  ):""}
        </div>
    
    ) 
}
export default AddCustomer;            