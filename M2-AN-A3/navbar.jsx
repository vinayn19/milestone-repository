import React, { Component } from "react";
import {Link} from "react-router-dom";
import auth from "./services/authService";
class Navbar extends Component {

    state={}

    render() {
        let {user} = this.props;
        return (
            <nav className="navbar navbar-expand-sm navbar-light bg-success" >
                <Link className="navbar-brand" to={`/login`} >Home</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>

                <ul className="navbar-nav">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/register`}>Register </Link>
                    </li>
                </ul>
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown">
                        Assign
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/admin/addemp">Add Employee</Link>
                        <Link className="dropdown-item" to="/admin/viewemp">View Employees</Link>
                    </div>
                </li>
                </ul> 
                <ul className="navbar-nav">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle" href="#"  data-toggle="dropdown">
                        View
                    </a>
                    <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <Link className="dropdown-item" to="/allStudents?page=1">All Students</Link>
                        <Link className="dropdown-item" to="/admin/viewemp">All Faculties</Link>
                    </div>
                </li>
                </ul> 
                <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                        <Link className="nav-link" to={`#`}>Welcome Admin </Link>
                    </li>
                </ul>
                {!user &&  
                    <ul className="navbar-nav ">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/login`} onClick={this.handleLogin}>LOGIN </Link>
                    </li>
                </ul>}
                {user && 
                    <ul className="navbar-nav ">
                        <li className="nav-item">
                        <Link className="nav-link" to={`/logout`} onClick={this.handleLogout}>LOGOUT </Link>
                    </li>
                </ul>}
            </nav>
        )
    }
}
export default Navbar;