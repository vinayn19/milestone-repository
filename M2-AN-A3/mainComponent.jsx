import React, {Component} from "react";
import Navbar from "./navbar";
import {Switch,Redirect,Route} from 'react-router-dom';
import auth from './services/authService';
import AdminHomePage from './adminHomePage';
import Login from './login';
import Logout from './logout';
import Register from './register';
import AllStudents from './allStudents';
import AllFaculties from "./allFaculties";

class MainComponent extends Component {
    state={};


    render() {
        let user = auth.getUser();
        return (
            <React.Fragment>
                <Navbar user={user}/>
                <Switch>
                <Route path='/login' render={props=> <Login />}/>
                <Route path='/logout' render={props=> <Logout user={user}/>}/>
                <Route path='/admin' render={props=> <AdminHomePage user={user}/>}/>
                <Route path='/register' render={props=> <Register user={user} {...props}/>}/>
                <Route path='/allStudents' render={props=> <AllStudents user={user} {...props}/>}/>
                {/* <Route path='/allStudents' render={props=> <AllStudents user={user} {...props}/>}/> */}
                <Redirect path="/" to="/login"/>


                </Switch>
            </React.Fragment>
        )
    }
}
export default MainComponent;