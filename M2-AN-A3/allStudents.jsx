import React, {Component} from "react";
import queryString from "query-string";
import http from "./services/httpService";

class allStudents extends Component {
    state={
        allStudents:[],
        allCourses:[],
    };

    async componentDidMount() {
        const [response1,response2] = await Promise.all([http.get(`/getStudents?page=1`),http.get(`/getCourses`)]);
        this.setState({allStudents:response1.data, allCourses:response2.data});
    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps !== this.props) {
        let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            let response = await http.get(`/getStudents${queryStr}`);
            let {data} = response;
            this.setState({allStudents:data});
        }
    }
    makeQueryString=(queryParams)=> {
        let {page,course} = queryParams;
        let queryString = "";
        queryString = this.addToQueryString(queryString,"page",page);
        queryString = this.addToQueryString(queryString,"course",course);
        return queryString;
    }

        addToQueryString=(queryString,queryParam,queryValue)=>
            queryValue ?
             queryString ?  queryString + `&${queryParam}=${queryValue}`:`?${queryParam}=${queryValue}`
             : queryString;

        

    callUrl=(queryParams)=> {
        let baseUrl = `/allStudents`;
        let queryString = this.makeQueryString(queryParams);
        this.props.history.push({
            pathname: baseUrl,
            search:queryString,
        })
     } 

     handleChange=(e)=>{
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        let queryParams = queryString.parse(this.props.location.search);
        queryParams.page = 1;
        queryParams[input.name] = this.updateCBs(queryParams[input.name],input.checked,input.value);
        this.callUrl(queryParams);

    }
    
    updateCBs=(inpValues, checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else {
            let index = inpArr.findIndex(ele=>ele===value);
            if(index>=0) inpArr.splice(index,1);
        }
        return inpArr.join(",");
    }
    
    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {page} = queryParams;
        queryParams.page = +page + incr;
        this.callUrl(queryParams);
    }


    render() {
        let {allStudents,allCourses} = this.state;
        console.log(allStudents,allCourses);
        let {page,items=[],totalItems,totalNum} = allStudents;
        let allCourseNames = allCourses.map(course=> course.name);
        let itemsOnPage = 3;
        let totalPages = Math.ceil(+totalNum/itemsOnPage);
        let startPageIndex = (+page-1) * +itemsOnPage + 1;
        let finalPageIndex = startPageIndex + +itemsOnPage -1 <= totalNum ? startPageIndex + +itemsOnPage -1: totalNum ; 

        let queryParams = queryString.parse(this.props.location.search);
        let {course=''} = queryParams;
        return (
            <div className='container'>
                <div className="row">
                    <div className="col-3">
                        {this.makeCheckbox('Options',allCourseNames,'course',course.split(','))}
                    </div>
                    <div className="col-9 text-center">
                        <h3 className='text-left'>All Students</h3>
                   <div className='text-left'>{startPageIndex}-{finalPageIndex} of {totalNum}</div> 
                        <div className="row border border">
                            <div className="col-1">Id</div>
                            <div className="col-2">Name</div>
                            <div className="col-2">Date of Birth</div>
                            <div className="col-2">About</div>
                            <div className="col-5">Courses</div>
                        </div>
                        {items.map(stud=> (
                        <div className="row border bg-warning">
                            <div className="col-1">{stud.id}</div>
                            <div className="col-2">{stud.name}</div>
                            <div className="col-2">{stud.dob}</div>
                            <div className="col-2">{stud.about}</div>
                            <div className="col-5">{stud.courses.join(',')}</div>
                        </div>))}
                        <div className="row my-3">
                        <div className="col-1">
                        {page > 1 && 
                        <button className="btn btn-secondary" onClick={()=>this.changePage(-1)}>Previous</button>}
                        </div>
                        <div className="col-10"></div>
                        
                        <div className="col-1">
                        {page < totalPages &&
                        <button className="btn btn-secondary" onClick={()=>this.changePage(+1)}>Next</button>}
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }



 
makeCheckbox = (label,arr,name,value) => (
    <div className="container-fluid border">
            <label className="m-0 font-weight-bold">{label}</label>
            {arr.map(opt=> (
             <div className='border'>
             <div class="form-check p-2">
                <input 
                class="form-check-input" 
                type="checkbox" 
                name={name} 
                value={opt}
                checked={value.find(val=> val===opt) ? true: false}
                onChange={this.handleChange}
                />
                <label class="form-check-label" for="flexCheckDefault">
                    {opt}
                </label>
            </div>
            </div>
                
            ))}
    </div>
    )
}
export default allStudents;