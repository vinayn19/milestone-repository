import React, {Component} from "react";
import http from "./services/httpService";
class Register extends Component {
    state={
        roleArr:['Student','Faculty'],
        userInfo:{},
        formErrors:{},
    };

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget:input} = e;
        if(input.name==='confirmPassword')
        s1[input.name] = input.value;
        else 
        s1.userInfo[input.name] = input.value;

        s1.formErrors[input.name] = this.makeFormErrors(input.name,input.value); 

        this.setState(s1);
    }

    makeFormErrors=(name,value)=> {
        switch(name) {
            case 'name':
            return value
            ? ''
            :'Enter the name';

            case 'password':
            return value  
            ? value.length>=7
            ?''
            :'Password length should be minimum 7 characters'
            :'Enter a password'

            case 'confirmPassword':
                return value  
                ? value===this.state.userInfo.password
                ?''
                :'Re-Enter current password'
                :'Pasword does not match'

            case 'email':
                return value 
                ? value.includes('@') && value.includes('.com') 
                ? ''
                : 'Not a valid email id' 
                : 'Email is mandatory'


        }

    }


    registerUser= async()=> {
        let response = await http.post('/register',this.state.userInfo);
        let {data} = response;
        if(response.status===200) {
            alert('User created successfully!');
            this.props.history.push('/admin');
        } 
    }


    render() {
        let {roleArr,userInfo,confirmPassword,formErrors={}} = this.state;
        let {name,password,email,role} = userInfo;
        return (
            <div className='container'>
                {this.makeTextBox('Name','Enter your name','name',name,formErrors.name)}
                {this.makeTextBox('Password','Enter your password','password',password,formErrors.password)}
                {this.makeTextBox('Confirm Password','Re-Enter your password','confirmPassword',confirmPassword,formErrors.confirmPassword)}
                {this.makeTextBox('Email','Enter your email','email',email,formErrors.email)}
                {this.makeRadios('Role',roleArr,'role',role)}
                <button className="btn btn-primary" onClick={this.registerUser}>Register</button>
            </div>
        )
    }


    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid">
            <label className="m-0">{label}</label>
            {arr.map(opt=> (
            <div class="form-check">
                <input 
                class="form-check-input" 
                type="radio" 
                name={name} 
                value={opt}
                checked={value===opt}
                onChange={this.handleChange}
                />
                <label class="form-check-label" for="flexRadioDefault1">
                    {opt}
                </label>
            </div>
    
            ))}
    
        </div>
    )


    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group my-3">
            <label>{label}</label> <span className='text-danger'>*</span>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
            {error && <div className="text-danger alert alert-danger py-1" role="alert" >{error}</div> }

        </div>
    
    )
}
export default Register;