import React, {Component} from "react";
import {Switch,Route,Redirect} from "react-router-dom";
import Navbar from "./navbar";
import BooksSection from "./booksSection";
import Book from "./book";
import BookForm from "./bookForm";
class MainComponent extends Component {
    state={
        navSelect:-1,
    };

    selectedNavbar=(navNum)=> {
        this.setState({ navSelect:navNum});

    }



    render() {
        let {navSelect} = this.state;
        return (
            <div className="container-fluid p-0">
            <Navbar navSelect={navSelect} selectedNavbar={this.selectedNavbar}/>   
                <Switch>
                    
                    <Route path={"/books/new"} render={props=> <BookForm {...props} selectedNavbar={this.selectedNavbar}/>}/>
                    <Route path={"/books/all"} component={BooksSection}/>
                    <Route path={`/book/:bookid`} component={Book}/>
                    <Route path={"/books/:genre"} component={BooksSection}/>
                    <Route path={"/books"} component={BooksSection}/>
                    <Redirect from={'/'} to={'/books/all'}/>
                </Switch>

            </div>
        )
    }
    
}
export default MainComponent;