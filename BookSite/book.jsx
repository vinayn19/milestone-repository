import React, {Component} from "react";
import http from "./httpService";
class Book extends Component {
    state={
        data:{},
    };

    async componentDidMount() {
        let {bookid} = this.props.match.params;
        let response = await http.get(`/booksapp/book/${bookid}`)
        let {data} = response;
        this.setState({data:data});
    }


    render() {
        let {data={}} = this.state;
        let {name,author,genre,description,blurb,review,price,avgrating} = data;
        return (
            <div className="container">
                <h3>Book:{name}</h3>
            <div className="row">
                <div className="col-3 border">Author</div>
                <div className="col-7 border">{author}</div>
                <div className="col-3 border">Genre</div>
                <div className="col-7 border">{genre}</div>    
                <div className="col-3 border">Description</div>
                <div className="col-7 border">{description}</div>
                <div className="col-3 border">Blurb</div>
                <div className="col-7 border">{blurb}</div>
                <div className="col-3 border">Review</div>
                <div className="col-7 border">{review}</div>
                <div className="col-3 border">Price</div>
                <div className="col-7 border">{price}</div>
                <div className="col-3 border">Rating</div>
                <div className="col-7 border">{avgrating}</div>
            </div>

            </div>
        )
    }
}
export default Book;