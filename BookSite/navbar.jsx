import React, {Component} from "react";
import {Link} from "react-router-dom";
class Navbar extends Component {
   

    selectedNavbar=(navNum)=> {
        this.setState({ navSelect:navNum});

    }


    render() {
        let {selectedNavbar,navSelect} = this.props;
        let active1="",active2="",active3="",active4="",active5="",active6="",active7="";
         switch (navSelect) {
            case 0:
                active1 = "active";  
                break;
            case 1:
                active2 = "active";  
                break;
            case 2:
                active3 = "active";  
                break;
            case 3:
                active4 = "active";  
                break;  
            case 4:
            active5 = "active";  
                break;   
            case 5:
            active6 = "active";  
                break;                 
            case 6:
            active7 = "active";  
                break;   
            default:
                break;
        }


        return (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <Link className="navbar-brand " to={`/`} onClick={()=>selectedNavbar(5)}>BookSite</Link>
                <button className="navbar-toggler" type="button" >
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className={"nav-link " + active1 } to={`/books?newarrival=yes`}
                            onClick={()=>selectedNavbar(0)} >New Arrivals</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active2 } to={`/books/children`}
                            onClick={()=>selectedNavbar(1)}>Children</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active3 } to={`/books/fiction`}
                            onClick={()=>selectedNavbar(2)}>Fiction</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active4 } to={`/books/mystery`}
                            onClick={()=>selectedNavbar(3)}>Mystery</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active5 } to={`/books/management`}
                            onClick={()=>selectedNavbar(4)}>Management</Link>
                        </li>
                        <li className="nav-item">
                            <Link className={"nav-link " + active6 } to={`/books/all`}
                            onClick={()=>selectedNavbar(5)}>All Books</Link>
                        </li>
                        <li className="nav-item" style={{marginLeft:"550px"}}>
                            <Link className={"nav-link " + active7 } to={`/books/new`}
                            onClick={()=>selectedNavbar(6)}>New Book</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}
export default Navbar;