import React, {Component} from "react";
import http from "./httpService";
class BookForm extends Component {
    state={
        genreArr:["Fiction","Mystery","Management","Children","Self Help"],
        langsArr:["English","Latin","French","Other"],
        bestSelArr:["Yes","No"],
        newArrivalArr:["Yes","No"],
        bookData:{},
        bookDataErrors:{},
    };

    handleChange=(e)=> {
        let s1 = {...this.state};
        let {currentTarget: input} = e;
        s1.bookData[input.name] = input.value;
        s1.bookDataErrors[input.name]= this.makeBookDataErrors(input.name,input.value);
        this.setState(s1);
    }

    makeBookDataErrors=(inpName,inpValue)=>{
        let inpNameCapitalize =inpName.charAt(0).toUpperCase() + inpName.slice(1); 
       if(inpNameCapitalize==="Description") 
            return this.makeDescError(inpNameCapitalize ,inpValue);
        else if(inpNameCapitalize==="Genre" || inpNameCapitalize==="Language" ) 
            return inpValue ? "":`Please select the ${inpNameCapitalize}`;
         else 
           return inpValue ? "":`${inpNameCapitalize} is mandatory`;
    }

    makeDescError=(name,value)=> {
        return value ? 
            value.length<30 ? 
            `${name} should be atleast 30 characters long`
            :""
            :`${name} is mandatory`;
    }

    validateForm=()=> {
        let {bookData,bookDataErrors} = this.state;
         if(Object.keys(bookData).length === 13) 
           return !Object.values(bookDataErrors).find(error=> error);
         
    }

     handleCreateBook=()=>{
       this.submitBook();
    }

   async submitBook(){
        let response = await http.post(`/booksapp/book`,this.state.bookData);
        this.props.selectedNavbar(5);
        this.props.history.push(`/booksapp/books`);
    }

    render() {
        let {genreArr,langsArr,bestSelArr,newArrivalArr,bookDataErrors} = this.state;
        let {name,author,description,blurb,price,year,publisher,avgrating,genre,language,bestseller,newarrival,review} = this.state.bookData;
       
        return (
            <div className="container mt-4 w-75 text-center" style={{marginLeft:"100px"}}>
                <h3  className="mb-4">Create a New Book</h3>
                {this.makeTextBox("Name","Enter name","name",name,bookDataErrors.name)}
                {this.makeTextBox("Author","Enter author","author",author,bookDataErrors.author)}
                {this.makeTextBox("Description","Enter some description","description",description,bookDataErrors.description)}
                {this.makeTextBox("Blurb","Enter blurb","blurb",blurb,bookDataErrors.blurb)}
                {this.makeTextBox("Review","Enter Review","review",review,bookDataErrors.review)}
                {this.makeTextBox("Price","Enter price","price",price,bookDataErrors.price)}
                {this.makeTextBox("Year","Enter year","year",year,bookDataErrors.year)}
                {this.makeTextBox("Publisher","Enter Publisher","publisher",publisher,bookDataErrors.publisher)}
                {this.makeTextBox("Rating","Enter rating","avgrating",avgrating,bookDataErrors.avgrating)}
                {this.makeDropDowns("Genre",genreArr,genre,"genre","Select Genre",bookDataErrors.genre)}
                {this.makeDropDowns("Language",langsArr,language,"language","Select Language",bookDataErrors.language)}
                {this.makeRadios("Best Seller",bestSelArr,"bestseller",bestseller)}
                {this.makeRadios("New Arrival",newArrivalArr,"newarrival",newarrival)}
                <button className="btn text-white mt-3 mb-5" style={{backgroundColor:"#65909A"}}
                disabled={!this.validateForm()} onClick={this.handleCreateBook}>Create Book</button>
            </div>
        )
    }

    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <div className="row">
                <div className="col-3">
                  <label>{label}</label>

                </div>
                <div className="col-9">
                <input
                    type={name==="price" ?"number": "text"}
                    className="form-control"
                    name={name}
                    value={value}
                    placeholder={placeholder}
                    onChange={this.handleChange}
                 />
                     {error ? (<div className="text-danger text-left">
                       <small>{error}</small>  
                    </div>  ):""}
                </div>
            </div>
        </div>
    
    )

    makeDropDowns = (label,arr,value,name,header,error="") => (
        <div className="row">
            <div className="col-3">{label}</div>
            <div className="col-9">
                <div className="form-group">
                    <select
                        className="form-control"
                        name={name}
                        value={value}
                        onChange={this.handleChange}
                    >
                            <option value=""> {header} </option>
                            {arr.map(opt => (<option >{opt}</option>))}
            
                    </select>
                {error ? <div className="text-danger text-left"><small>{error}</small> </div>:"" }

                </div>
            </div>
        </div>
    )

    makeRadios=(label,arr,name,value="")=> (
        <div className="container-fluid mt-2">
            <div className="row">
                <div className="col-3 px-0">
                    <label>{label}</label>
                </div>
                <div className="col-9">
                    <div className="row">
                    {arr.map(opt=> (
                            <div className="col-6">
                                <div className="form-check">
                                    <input 
                                    className="form-check-input" 
                                    type="radio" 
                                    name={name} 
                                    value={opt}
                                    checked={value===opt}
                                    onChange={this.handleChange}
                                    />
                                    <label className="form-check-label" >
                                        {opt}
                                    </label>
                                </div>

                            </div>
            
                     ))}

                     </div>
                </div>
            </div>
    
        </div>
    )
    
    
}
export default BookForm;