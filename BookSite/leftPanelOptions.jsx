import React, {Component} from "react";
class MainComponent extends Component {
    state={
        langsArr:["English","Latin","French","Other"],
        bestSelArr:["Yes","No"],
        cbNumbers:{},
    };


    updateCBs=(inpValues, checked,value)=>{
        let inpArr = inpValues ? inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else {
            let index = inpArr.findIndex(ele=>ele===value);
            if(index>=0) inpArr.splice(index,1);
        }
        return inpArr.join(",");
    }


    handleChange=(e) => {
        let options = this.props.options;
        let {currentTarget: input} = e;
        options[input.name] = this.updateCBs(options[input.name],input.checked,input.value);
        options.page= "1";
        this.props.handleOptionChange(options);
    }

    render() {
        let {langsArr,bestSelArr} = this.state;
        let {refineOptions={},options} = this.props;
        console.log("refineOptions",refineOptions)
        let {bestseller="",language=""} = options;
        return (
            <React.Fragment>
                <strong>Choose Filter Options</strong>
              {this.makeCheckbox("BestSeller",bestSelArr,"bestseller",bestseller.split(","),refineOptions.bestseller)}  
              {this.makeCheckbox("Language",langsArr,"language",language.split(","),refineOptions.language)}  
            </React.Fragment>
        )
    }


    
    makeCheckbox = (label,arr,name,values,refineArr) => (
        <div className="container-fluid pr-0">
                <label className="m-0">{label}</label>
                {arr.map(opt=> (
                    
                <div class="form-check">
                    <input 
                    class="form-check-input" 
                    type="checkbox" 
                    name={name} 
                    value={opt}
                    checked={values.find(val=> val===opt) ? true: false}
                    onChange={this.handleChange}
                    />
                    <label class="form-check-label">
                        {opt} 
                    </label>
                     ({refineArr ? refineArr.find(ele=>ele.refineValue===opt).totalNum:""})
                </div>
                    
                ))}
        </div>
    )
}
export default MainComponent;