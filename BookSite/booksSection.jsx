import React, {Component} from "react";
import http from "./httpService";
import queryString from "query-string";
import {Link} from "react-router-dom";
import LeftPanelOptions from "./leftPanelOptions";
class BooksSection extends Component {
    state={
        data:{},
    };


   async componentDidMount() {
        let apiURL = "/booksapp/books";
        let response = await http.get(apiURL);
        let {data} = response;
        console.log(data);
        this.setState({data: data});
    }

    async componentDidUpdate(prevProps,prevState) {
        if(prevProps!==this.props) {
            let {genre} = this.props.match.params;
            let apiURL = "/booksapp/books";
            let queryParams = queryString.parse(this.props.location.search);
            let queryStr = this.makeQueryString(queryParams);
            apiURL = genre && genre!=='all' ? apiURL + `/${genre}`:  apiURL;
            apiURL = apiURL + queryStr;
            let response = await http.get(apiURL);
            let {data} = response;
            console.log(data);
            this.setState({data: data});

        }

    }

    handleOptionChange=(options)=> {
        this.callURL(options);
    }

    makeQueryString=(options)=> {
        let {newarrival,page="1",bestseller,language} = options;
        let queryString = "";
        queryString = this.addToQueryString(queryString,"newarrival",newarrival);
        queryString = this.addToQueryString(queryString,"bestseller",bestseller);
        queryString = this.addToQueryString(queryString,"language",language);
        queryString = this.addToQueryString(queryString,"page",page);
        return queryString;
    }

    addToQueryString=(queryString,queryParam,queryValue)=> {
        if(queryValue) 
            return queryString ? queryString + `&${queryParam}=${queryValue}`:
            queryString + `?${queryParam}=${queryValue}`;
        else return queryString;
    }

    changePage=(incr)=> {
        let queryParams = queryString.parse(this.props.location.search);
        let {page="1"} = queryParams;
        queryParams.page = +page + incr;
        this.callURL(queryParams);
    }

    callURL=(options)=> {
        let queryString = this.makeQueryString(options);
        let {genre} = this.props.match.params;
        let baseURL = "/books";
         baseURL = genre && genre!=='all' ? baseURL + `/${genre}`:  baseURL ;
        this.props.history.push({
            pathname:baseURL,
            search:queryString,
        })
    }

    render() {
        let {books=[],pageInfo={},refineOptions} = this.state.data;
        let {pageNumber,numberOfPages,numOfItems,totalItemCount} = pageInfo;
        let startIndex = (pageNumber-1)*10 +1;
        let endIndex = (startIndex + numOfItems)<=totalItemCount ? startIndex + numOfItems: totalItemCount;
        let queryParams = queryString.parse(this.props.location.search);
        return (
            <div className="container-fluid mt-3" style={{width:"95%"}}>
                <div className="row">
                    <div className="col-2 pr-0">
                         <LeftPanelOptions options={queryParams} handleOptionChange={this.handleOptionChange} refineOptions={refineOptions}/>
                    </div>
                    <div className="col-10">
                        {startIndex} to {endIndex} of {totalItemCount}
                        <div className="row text-white" style={{backgroundColor:"#65909A"}}>
                                <div className="col-4 border">Title</div>
                                <div className="col-3 border">Author</div>
                                <div className="col-1 border pl-2">Language</div>
                                <div className="col-2 border">Genre</div>
                                <div className="col-1 border">Price</div>
                                <div className="col-1 border">Best Sl</div>
                            </div>
                        {books.map(book=> (
                            <div className="row" key={book.bookid}>
                                <div className="col-4 border"><Link to={`/book/${book.bookid}`}>{book.name}</Link></div>
                                <div className="col-3 border">{book.author}</div>
                                <div className="col-1 border">{book.language}</div>
                                <div className="col-2 border">{book.genre}</div>
                                <div className="col-1 border">{book.price}</div>
                                <div className="col-1 border">{book.bestseller}</div>
                            </div>
                        ))}
                        <div className="row mt-2">
                            <div className="col-2">
                            {pageNumber>1? <button className="btn text-light" style={{backgroundColor:"#65909A"}} 
                                onClick={()=>this.changePage(-1)}>
                                Prev 
                                </button>:""}
                            </div>
                            <div className="col-8"></div>
                            <div className="col-2">
                                {pageNumber<numberOfPages ? <button className="btn text-light" style={{backgroundColor:"#65909A"}}
                                onClick={()=>this.changePage(+1)}> Next
                                </button>:""}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}
export default BooksSection;