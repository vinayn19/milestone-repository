import React, { Component } from "react";
class MyComp extends Component {
    state = {
        brands:[
            {category:"Food", brandNames:["Nestle", "Haldiram", "Pepsi", "Coca Cola", "Britannia", "Cadburys"]},
            { category: "Personal Care", brandNames: ["P&G", "Colgate", "Parachute", "Gillete", "Dove"]},
            {category:"Apparel",brandNames:["Levis", "Van Heusen", "Manyavaar", "Zara"]}
        ],
        products:[
            {
                code: "PEP1253", price: 20, brand: "Pepsi", category: "Food",
                specialOffer: false, limitedStock: false, quantity: 25
            },
            {
                code: "MAGG021", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 10
            },
            {
                code: "LEV501", price: 1000, brand: "Levis", category: "Apparel",
                specialOffer: true, limitedStock: true, quantity: 3
            },
            {
                code: "CLG281", price: 60, brand: "Colgate", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            },
            {
                code: "MAGG451", price: 25, brand: "Nestle", category: "Food",
                specialOffer: true, limitedStock: true, quantity: 0

            },
            {
                code: "PAR250", price: 40, brand: "Parachute", category: "Personal Care",
                specialOffer: true, limitedStock: true, quantity: 5
            }
        ],
        categories:["Food", "Personal Care", "Apparel"],
        product: {limitedStock:false, specialOffer:false, quantity:0},
        view: -1,
        stockReceived: {codeStock:"", stockQuantity:""},
        editIndex : -1,
        errors:{code:"" ,price:""}
        /* -1 for normal products view
            0 for New Product form 
            1 receive Stock
        */
            
    }

    addNewProductForm =(index)=> {
        let s1 = {...this.state};
        s1.view = 0;
        this.setState(s1);
    }

    handleChange=(e)=> {
        let s1 = { ...this.state };
        let {currentTarget: input} = e;
        // if(input.type==="checkbox") s1.product[input.name] =input.value ;
        if(input.name==="codeStock" || input.name=="stockQuantity") s1.stockReceived[input.name] = input.value;
        else  s1.product[input.name] = input.value;

        this.setState(s1);

    }

    handleChangeCB=(e)=> {
         let s1 = { ...this.state };
         let {currentTarget:input} = e;
        s1.product[input.name] = !s1.product[input.name] ; 
        this.setState(s1);

    }
        
    gotoHomepage=()=> {
        let s1 = { ...this.state };
        s1.view = -1;
        this.setState(s1);
    }

    addProduct=()=> {
        let s1 = { ...this.state };
        s1.errors.code = !s1.product.code? "Product code is mandatory":"";
        s1.errors.price = ! s1.product.price? "Price is mandatory":"";
        if(!s1.errors.code && !s1.errors.price){
            
            if(s1.editIndex===-1) {
                
                s1.products.push(s1.product);
            } else {
                s1.products[s1.editIndex] = s1.product;
            }
            s1.editIndex= -1;
            s1.product = { limitedStock: false, specialOffer: false, quantity:0 };
            s1.view = -1;
        }
        this.setState(s1);
    }

    editProduct = (index) => {
        let s1 = { ...this.state };
        s1.view = 0;
        s1.editIndex = index;
        s1.product = s1.products[index];
        this.setState(s1);

    }


    receiveStocks =()=> {
        let s1 = { ...this.state };
        s1.view= 1;
        this.setState(s1);

    }

    addStocks=()=> {
        let s1 = { ...this.state };
        let  code = s1.stockReceived.codeStock;
        let receivedQty = s1.stockReceived.stockQuantity;
        if (!code) alert("Please Select the Product code");
        else if (!receivedQty) alert("Please Enter the Product quantity");
        else {
            let currentQty = s1.products.find(prod => prod.code === code).quantity ; 
            s1.products.find(prod => prod.code === code).quantity=   +currentQty + +receivedQty ;
            s1.view = -1;

        }
        this.setState(s1);
    }
    
    

    render() {
        let { products, brands, view,categories,offers, product,stockReceived,errors,editIndex} = this.state;
        let {code, price, brand,category, specialOffer, limitedStock, quantity } = product;
        let {codeStock,stockQuantity} = stockReceived;
        let codes = products.map(prod=> prod.code);
       let  filterBrands = category ? brands.find(br => br.category === category).brandNames :[...brands[0].brandNames,...brands[1].brandNames,...brands[2].brandNames] ;
        return (
            <React.Fragment>
                <div className="container">
                {this.makeNavbar(products)}

                </div>
                {view===-1 ? (
                    
                    <div className="container">
                        <div className="row my-4">
                            {products.map((prod,index)=> (
                                <div className="col-3 bg-light text-center border">
                                    <h6>Code: {prod.code}</h6>  
                                    Brand: {prod.brand} <br/>
                                    category: {prod.category} <br/>
                                    Price: {prod.price} <br/>
                                    Quantity:{prod.quantity} <br/>
                                    Special Offers: {prod.specialOffer.toString()} <br/>
                                    Limited Stock: {prod.limitedStock.toString()} <br/>
                                    <button className="btn btn-warning" onClick={()=> this.editProduct(index)}>Edit Details</button>
                                </div>
                            ))}
                        </div>
                        <button className="btn btn-primary mx-2" onClick={this.addNewProductForm}>Add New Product</button>
                        <button className="btn btn-primary" onClick={this.receiveStocks}>Receive Stocks</button>
                    </div>
                ): (view===0 ? (
                    <div className="container my-4">

                            {this.makeTextBox("Product Code", "Enter product Code", "code",code,errors.code)}
                            {this.makeTextBox("Price","Enter price", "price",price,errors.price)}
                            {this.makeRadios("Category",categories,"category",category)}
                            {this.makeDropDowns("Select Brand", filterBrands,"brand",brand)}
                            <label className="form-check-label font-weight-bold">
                                Choose other info about the product
                            </label> <br />
                            {this.makeCheckbox("Special Offer", "specialOffer", specialOffer)}
                            {this.makeCheckbox("Limited Stock" ,"limitedStock", limitedStock)}
                            <button className="btn btn-primary mx-2" onClick={this.addProduct}>{editIndex === -1 ? "Add Product" :"Edit Product"}</button>
                            <button className="btn btn-primary" onClick={this.gotoHomepage}>Go Back to Homepage</button>
                    </div>
                ):view===1 ? (
                    <div className="container my-4">
                        <h3>Select the product whose stocks have been received</h3>
                                {this.makeDropDowns("Select Product Code",codes, "codeStock",codeStock)}
                                {this.makeTextBox("Stocks Received","Enter Stocks","stockQuantity",stockQuantity)}
                                <button className="btn btn-primary mt-3 mx-2" onClick={this.addStocks}>Submit</button>
                                <button className="btn btn-primary mt-3" onClick={this.gotoHomepage}>Go Back to Home Page</button>
                    </div>
                ): "")}

            </React.Fragment>
        )
    }
/* ************************* */
    


    makeNavbar=(products)=> {
        let qty = products.reduce((acc, curr) => acc +  curr.quantity, 0);
        let value = products.reduce((acc,curr)=> acc + curr.quantity* curr.price,0);
        return (
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">ProdStoreSys</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Products
                            <span className="badge badge-secondary mx-2">{products.length}</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Quantity
                            <span className="badge badge-secondary mx-2">{qty}</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Value
                            <span className="badge badge-secondary mx-2">{value}</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }


    makeRadios = (label, arr, name, selVal) => {
        return (
            <React.Fragment>
                <label className="form-check-label font-weight-bold">
                    {label}
                </label> <br />

                {arr.map(opt => (
                    <div className="form-check">
                        <input
                            type="radio"
                            className="form-check-input"
                            name={name}
                            value={opt}
                            checked={selVal === opt}
                            onClick={this.handleChange}
                        />
                        <label className="form-check-label">{opt}</label>
                    </div>
                ))}
            </React.Fragment>
        )
    }


    makeTextBox = (label, placeholder, name,value,error) => (
        <div className="form-group">
            <label>{label}</label>
            <input
                type={name==="price" ?"number": "text"}
                className="form-control"
                name={name}
                value={value}
                placeholder={placeholder}
                onChange={this.handleChange}
            />
            {error ? (<div className="alert alert-danger" role="alert">{error}</div>  ):""}
        </div>

    )

    makeCheckbox = (label,  name, selValue) => {
        return (
            <React.Fragment>
            
                    <div className="form-check">
                        <input
                            type="checkbox"
                            className="form-check-input"
                            name={name}
                            // value={!selValue}
                            checked={selValue}
                            onChange={this.handleChangeCB} />
                        <label className="form-check-label">{label}</label>
                    </div>
            </React.Fragment>

        )
    }


    makeDropDowns = ( header, arr, name, selValue) => {
        return (
            <div className="form-group mt-2">
               
                <select className="form-control" value={selValue} name={name} onChange={this.handleChange}>
                    <React.Fragment>
                        <option selected value="">{header}</option>
                        {arr.map(a => (<option value={a} >{a}</option>))}

                    </React.Fragment>
                </select>
            </div>
        )
    }

}
export default MyComp;